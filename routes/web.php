<?php

use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login.login');
})->name('login-dental');

Route::get('/registro', 'PacienteController@create')->name('registro');

Route::group(['middleware' => 'auth'], function() {
    Route::resource('detalle-paciente', 'DetallePacienteController');
    Route::resource('cita', 'CitaController');
    Route::resource('paciente', 'PacienteController');
});

Route::get('hitoria-paciente', function () {
    return view('historia-paciente.index');
})->name('historia-paciente');

Auth::routes();

Route::get('/cita/primera/index', 'PrimeraCitaController@index')
    ->name('primera.cita');
Route::get('/cita/insertar', 'CitaController@ingresar_cita')
    ->name('ingresar.cita');
Route::get('/cita', 'CitaController@index')
    ->name('cita.index');

Route::get('/home', 'HomeController@index')->name('home');


Route::get('role-push', function () {
    $admin = Bouncer::role()->firstOrCreate([
        'name' => 'admin',
        'title' => 'Administrator',
    ]);
    $paciente = Bouncer::role()->firstOrCreate([
        'name' => 'Paciente',
        'title' => 'Paaciente',
    ]);
    $odontologo = Bouncer::role()->firstOrCreate([
        'name' => 'odontologo',
        'title' => 'Odontologo',
    ]);
    $createPrimeraCita = Bouncer::ability()->firstOrCreate([
        'name' => 'primera-cita-odontograma',
        'title' => 'Primera cita',
    ]);
    $createAgendarCita = Bouncer::ability()->firstOrCreate([
        'name' => 'agendar-cita',
        'title' => 'Agendar cita',
    ]);

    Bouncer::allow($admin)->to($createPrimeraCita);
    Bouncer::allow($admin)->to($createAgendarCita);

    (\App\User::findOrFail(1))->assign('admin');
    Bouncer::allow(\App\User::findOrFail(1))->to('primera-cita-odontograma');
});
