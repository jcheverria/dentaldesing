<?php

namespace Tests\Feature;

use App\Paciente;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Routing\Route;
use Tests\TestCase;
use App\User;

class HistoriaClinicaTest extends TestCase
{
    use RefreshDatabase;/* migra la base antes de ejecutar las pruebas
                        y hace un rollback a la base, despues de cada funcion de prueba */

    /** @test */
    function muestra_historia_clinica_de_un_paciente(){
        $paciente = factory(Paciente::class)->create([
            'primer_nombre' => 'Jorge'
        ]);

        $this->get('/usuarios'.$paciente->id)
            ->assertStatus(200)
            ->assertSee('Jorge');

        $this->markTestSkipped('incompleta');


    }

    /** @test */
    function carga_pagina_para_editar_historia_clinica_de_paciente () {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();

        $this->get("/historia/{$user->id}/edit") // usuarios/5/editar
            ->assertStatus(200)
        ->assertViewIs('historia-clinica.edit')
        ->assertSee('Editar Historia clinica');
    }

    /** @test */
    function graba_historia_clinica()
    {
        $this->withoutExceptionHandling();

        $this->put('algo', [  //creo que sería un PUT
            'presion_arterial' => '120-80',
            'frecuencia_cardiaca' => '100 bpm',
            'temperatura' => '37',
            'frecuencia_respiratoria' => '100bpm'



        ]);

          /*  ->assertRedirect(route('login-dental'));

        $this->assertDatabaseHas('pacientes', [
            'primer_nombre' => 'Jorge',
            'email' => 'jorgedpg10@gmail.com'
        ]);

        $this->assertDatabaseHas('detalles_paciente', [
            'motivo_consulta' => 'Me duele la mejilla'
        ]);

        $this->assertCredentials([
            'email' => '0917663333', //la cedula de paciente se guarda como email en la tabla Users
            'password' => '12345',
        ]);*/
    }








}
