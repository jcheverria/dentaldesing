<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Routing\Route;
use Tests\TestCase;
use App\User;

class PacienteTest extends TestCase
{
    use RefreshDatabase; /*migra la base antes de ejecutar las pruebas
                        y hace un rollback a la base, despues de cada funcion de prueba */

    /** @test */
    function muestra_lista_de_pacientes(){
        $this->get('historia')
            ->assertStatus(200)
            ->assertSee('Pacientes');
            //->assertSee('Joel')
    }

    /** @test */
    function it_stores_paciente_info()
    {
        $this->withoutExceptionHandling();

        $this->post('paciente', [
            'primer_nombre' => 'Jorge',
            'segundo_nombre' => 'David',
            'primer_apellido' => 'Peralta',
            'segundo_apellido' => 'Godoy',
            'cedula' => '0917663333',
            'email' => 'jorgedpg10@gmail.com',
            'password' => '12345',
            'edad' => 27,
            'sexo' => 'M',
            'motivo_consulta' => 'Me duele la mejilla',
            'antibiotico' => '1',
            'anestesia' => '1',
            'hemorragias' => '1',
            'sida' => '1',
            'tuberculosis' => '1',
            'asma' => '1',
            'diabetes' => '1',
            'hipertension' => '1',
            'cardiacas' => '1',
            'otros' => '1'

        ])->assertRedirect(route('login-dental'));

        $this->assertDatabaseHas('pacientes', [
            'primer_nombre' => 'Jorge',
            'email' => 'jorgedpg10@gmail.com'
        ]);

        $this->assertDatabaseHas('detalles_paciente', [
            'motivo_consulta' => 'Me duele la mejilla'
        ]);

        $this->assertCredentials([
            'email' => '0917663333', //la cedula de paciente se guarda como email en la tabla Users
            'password' => '12345',
        ]);
    }

    /** @test */
    function primer_nombre_es_obligatorio()
    {
        //$this->withoutExceptionHandling(); debo activar el manejo de excepciones para que
        // me deje mandar un post sin primer_nombre

        $this->from('registro')//usamos este from() para que funcione la asercion del redirect al encontrar el form incompleto
        ->post('paciente', [
            'primer_nombre' => '',
            'segundo_nombre' => 'David',
            'primer_apellido' => 'Peralta',
            'segundo_apellido' => 'Godoy',
            'cedula' => '0917663334',
            'email' => 'jorgedpg10@gmail.com',
            'password' => '12345',
            'edad' => 27,
            'sexo' => 'M',
            'motivo_consulta' => 'Me duele la mejilla',
            'antibiotico' => '1',
            'anestesia' => '1',
            'hemorragias' => '1',
            'sida' => '1',
            'tuberculosis' => '1',
            'asma' => '1',
            'diabetes' => '1',
            'hipertension' => '1',
            'cardiacas' => '1',
            'otros' => '1'

        ])->assertRedirect(route('registro'))
            ->assertSessionHasErrors(['primer_nombre']);

        $this->assertDatabaseMissing('users', [
            'email' => '0917663334'
        ]);
    }


    /** @test */
    function cedula_es_obligatorio()
    {
        //$this->withoutExceptionHandling(); debo activar el manejo de excepciones para que
        // me deje mandar un post sin primer_nombre

        $this->from('registro')//usamos este from() para que funcione la asercion del redirect al encontrar el form incompleto
        ->post('paciente', [
            'primer_nombre' => 'Jorge',
            'segundo_nombre' => 'David',
            'primer_apellido' => 'Peralta',
            'segundo_apellido' => 'Godoy',
            'cedula' => '',
            'email' => 'jorgedpg10@gmail.com',
            'password' => '12345',
            'edad' => 27,
            'sexo' => 'M',
            'motivo_consulta' => 'Me duele la mejilla',
            'antibiotico' => '1',
            'anestesia' => '1',
            'hemorragias' => '1',
            'sida' => '1',
            'tuberculosis' => '1',
            'asma' => '1',
            'diabetes' => '1',
            'hipertension' => '1',
            'cardiacas' => '1',
            'otros' => '1'

        ])->assertRedirect(route('registro'))
            ->assertSessionHasErrors(['cedula']);

        $this->assertEquals(0, User::count());
    }

    /** @test */
    function password_es_obligatorio()
    {
        //$this->withoutExceptionHandling(); debo activar el manejo de excepciones para que
        // me deje mandar un post sin primer_nombre

        $this->from('registro')//usamos este from() para que funcione la asercion del redirect al encontrar el form incompleto
        ->post('paciente', [
            'primer_nombre' => 'Jorge',
            'segundo_nombre' => 'David',
            'primer_apellido' => 'Peralta',
            'segundo_apellido' => 'Godoy',
            'cedula' => '',
            'email' => 'jorgedpg10@gmail.com',
            'password' => '',
            'edad' => 27,
            'sexo' => 'M',
            'motivo_consulta' => 'Me duele la mejilla',
            'antibiotico' => '1',
            'anestesia' => '1',
            'hemorragias' => '1',
            'sida' => '1',
            'tuberculosis' => '1',
            'asma' => '1',
            'diabetes' => '1',
            'hipertension' => '1',
            'cardiacas' => '1',
            'otros' => '1'

        ])->assertRedirect(route('registro'))
            ->assertSessionHasErrors(['password']);

        $this->assertEquals(0, User::count());
    }

}
