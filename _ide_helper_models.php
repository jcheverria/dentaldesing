<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\DetallePaciente
 *
 * @property int $id
 * @property string $motivo_consulta
 * @property string|null $problema_actual
 * @property mixed|null $antecedentes
 * @property string|null $antecedentes_text
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Paciente $paciente
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DetallePaciente newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DetallePaciente newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DetallePaciente query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DetallePaciente whereAntecedentes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DetallePaciente whereAntecedentesText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DetallePaciente whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DetallePaciente whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DetallePaciente whereMotivoConsulta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DetallePaciente whereProblemaActual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DetallePaciente whereUpdatedAt($value)
 */
	class DetallePaciente extends \Eloquent {}
}

namespace App{
/**
 * App\Paciente
 *
 * @property int $id
 * @property string $primer_nombre
 * @property string $segundo_nombre
 * @property string $primer_apellido
 * @property string $segundo_apellido
 * @property string $cedula
 * @property string $email
 * @property string $password
 * @property int $edad
 * @property string $sexo
 * @property int $detalle_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\DetallePaciente|null $detalle_paciente
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paciente newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paciente newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paciente query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paciente whereCedula($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paciente whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paciente whereDetalleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paciente whereEdad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paciente whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paciente whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paciente wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paciente wherePrimerApellido($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paciente wherePrimerNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paciente whereSegundoApellido($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paciente whereSegundoNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paciente whereSexo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Paciente whereUpdatedAt($value)
 */
	class Paciente extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

