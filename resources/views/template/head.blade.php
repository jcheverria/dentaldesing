  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>DentalDesing | @yield('title')</title>


      <!-- NProgress -->
      <link href="{{ asset('css/nprogress.css') }}" rel="stylesheet">
      <!-- iCheck -->
      <link href="{{ asset('css/flat/green.css') }}" rel="stylesheet">

      <!-- bootstrap-progressbar -->
      <link href="{{ asset('css/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet">
      <!-- JQVMap -->
      <link href="{{ asset('css/jqvmap.min.css') }}" rel="stylesheet" />
      <!-- bootstrap-daterangepicker -->
      <link href="{{ asset('css/daterangepicker.css') }}" rel="stylesheet">



      <script src="https://kit.fontawesome.com/dfea41f182.js" crossorigin="anonymous"></script>

      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

      <!-- Custom Theme Style -->
      <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet">

      @yield('estilo')
    <link href="{{ asset('css/base.css') }}" rel="stylesheet">
  </head>
