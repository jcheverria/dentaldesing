<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>Menú</h3>
        <ul class="nav side-menu">
            @can('llenar-historia')
                <li><a href='{{ route('detalle-paciente.index') }}'><i class="fas fa-notes-medical"></i> Llenar Historia clínica
                    </a></li>
            @endcan
            @can('ver-historia-clinica')
                <li><a href='{{ route('historia-paciente') }}'><i class="fas fa-notes-medical"></i> Ver Historia clínica
                    </a></li>
            @endcan
            @can('agendar-cita')
                <li><a href='{{ route('cita.index') }}'><i class="fas fa-calendar-alt"></i> Agendar cita </a></li>
                @endcan
                </li>
        </ul>
    </div>

</div>
<!-- /sidebar menu -->
