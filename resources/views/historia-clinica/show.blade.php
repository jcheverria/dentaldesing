@extends('base')

@section('title', 'nuevo2')

@section('estilo')
    <link href="{{ asset('css/historia-style.css') }}" rel="stylesheet">
@endsection

@section('content')


    {{--<h4> paciente: {{$paciente->primer_nombre}} {{$paciente->primer_apellido}}</h4>
    <h6> su motivo de consulta es: {{$detalle_paciente->motivo_consulta}}</h6>
    <h6>el paciente loggueado es {{ Auth::user() }}</h6>--}}

<div class="">
    <div class="row">
        @if ($errors->any())

            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li> {{ $error }} </li>
                    @endforeach
                </ul>
            </div>
            </ul>
        @endif
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Historia clínica</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <!-- Smart Wizard -->
                    <h6>Paciente: {{$paciente->primer_nombre}} {{$paciente->segundo_nombre}} {{$paciente->primer_apellido}} {{$paciente->segundo_apellido}} </h6>
                    <div id="wizard" class="form_wizard wizard_horizontal">
                        <ul class="wizard_steps">
                            <li>
                                <a href="#step-1">
                                    <span class="step_no">1</span>
                                    <span class="step_descr">
                                        Información de consulta
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-2">
                                    <span class="step_no">2</span>
                                    <span class="step_descr">
                                        Signos vitales
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-3">
                                    <span class="step_no">3</span>
                                    <span class="step_descr">
                                        Examen del sistema estomatognático
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-4">
                                    <span class="step_no">4</span>
                                    <span class="step_descr">
                                        Odontograma
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-5">
                                    <span class="step_no">5</span>
                                    <span class="step_descr">
                                        Indicadores de salud bucal
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-6">
                                    <span class="step_no">6</span>
                                    <span class="step_descr">
                                        índices CPO-ceo
                                    </span>
                                </a>
                            </li>

                        </ul>

                        <div id="step-1">


                            <label><b>Problema dental actual:</b></label>
                            <p>{{  $detalle_paciente->problema_actual }}</p>

                        </div>

                        <div id="step-2">
                            <div class="row">
                                <div class="col-sm-6">
                                    <p class="font-weight-bold">Presión arterial: </p> <p>{{ $detalle_paciente->presion_arterial }}</p>
                                    <p class="font-weight-bold">Presión arterial: </p> <p>{{ $detalle_paciente->frecuencia_cardiaca }}</p>
                                </div>

                                <div class="col-sm-6">
                                    <p class="font-weight-bold">Frecuencia Cardiaca: </p> <p>{{ $detalle_paciente->temperatura }}</p>
                                    <p class="font-weight-bold">F. Respiratoria min. </p> <p>{{ $detalle_paciente->frecuencia_respiratoria }}</p>
                                </div>
                            </div>



                        </div>

                        <div id="step-3">
                            <label>Examen del sistema estomatognatico</label>

                            @php
                                $informacion_guardada = json_decode($detalle_paciente->patologias, true);
                            @endphp

                            <div class="row">
                                <div class="col-sm-6">
                                    @if ($informacion_guardada["labios"] == 1)
                                        <i class="far fa-check-square d-inline"></i>
                                        <p class="d-inline">1. labios</p>
                                        <br>
                                    @else
                                        <i class="far fa-square d-inline"></i>
                                        <p class="d-inline">1. labios</p>
                                        <br>
                                    @endif

                                        @if ($informacion_guardada["mejillas"] == 1)
                                            <i class="far fa-check-square d-inline"></i>
                                            <p class="d-inline">2. Mejillas</p>
                                            <br>
                                        @else
                                            <i class="far fa-square d-inline"></i>
                                            <p class="d-inline">2. Mejillas</p>
                                            <br>
                                        @endif

                                        @if ($informacion_guardada["maxilar_superior"] == 1)
                                            <i class="far fa-check-square d-inline"></i>
                                            <p class="d-inline">3. Maxilar superior</p>
                                            <br>
                                        @else
                                            <i class="far fa-square d-inline"></i>
                                            <p class="d-inline">3. Maxilar superior</p>
                                            <br>
                                        @endif

                                        @if ($informacion_guardada["maxilar_inferior"] == 1)
                                            <i class="far fa-check-square d-inline"></i>
                                            <p class="d-inline">4. Maxilar inferior</p>
                                            <br>
                                        @else
                                            <i class="far fa-square d-inline"></i>
                                            <p class="d-inline">4 . Maxilar inferior</p>
                                            <br>
                                        @endif

                                        @if ($informacion_guardada["lengua"] == 1)
                                            <i class="far fa-check-square d-inline"></i>
                                            <p class="d-inline">5. Lengua</p>
                                            <br>
                                        @else
                                            <i class="far fa-square d-inline"></i>
                                            <p class="d-inline">5. Lengua</p>
                                            <br>
                                        @endif

                                        @if ($informacion_guardada["paladar"] == 1)
                                            <i class="far fa-check-square d-inline"></i>
                                            <p class="d-inline">6. Paladar</p>
                                            <br>
                                        @else
                                            <i class="far fa-square d-inline"></i>
                                            <p class="d-inline">6. Paladar</p>
                                            <br>
                                        @endif

                                </div>

                                <div class="col-sm-6">
                                    @if ($informacion_guardada["piso"] == 1)
                                        <i class="far fa-check-square d-inline"></i>
                                        <p class="d-inline">7. Piso</p>
                                        <br>
                                    @else
                                        <i class="far fa-square d-inline"></i>
                                        <p class="d-inline">7. Piso</p>
                                        <br>
                                    @endif

                                    @if ($informacion_guardada["carrillos"] == 1)
                                        <i class="far fa-check-square d-inline"></i>
                                        <p class="d-inline">8. Carrillos</p>
                                        <br>
                                    @else
                                        <i class="far fa-square d-inline"></i>
                                        <p class="d-inline">8. Carrillos</p>
                                        <br>
                                    @endif

                                    @if ($informacion_guardada["glandulas_salivales"] == 1)
                                        <i class="far fa-check-square d-inline"></i>
                                        <p class="d-inline">9. Glándulas salivales</p>
                                        <br>
                                    @else
                                        <i class="far fa-square d-inline"></i>
                                        <p class="d-inline">9. Glándulas salivales</p>
                                        <br>
                                    @endif

                                    @if ($informacion_guardada["faringe"] == 1)
                                        <i class="far fa-check-square d-inline"></i>
                                        <p class="d-inline">10. Oro faringe</p>
                                        <br>
                                    @else
                                        <i class="far fa-square d-inline"></i>
                                        <p class="d-inline">10. Oro faringe</p>
                                        <br>
                                    @endif

                                    @if ($informacion_guardada["atm"] == 1)
                                        <i class="far fa-check-square d-inline"></i>
                                        <p class="d-inline">11. A.T.M.</p>
                                        <br>
                                    @else
                                        <i class="far fa-square d-inline"></i>
                                        <p class="d-inline">11. A.T.M.</p>
                                        <br>
                                    @endif

                                    @if ($informacion_guardada["ganglios"] == 1)
                                        <i class="far fa-check-square d-inline"></i>
                                        <p class="d-inline">12. Ganglios</p>
                                        <br>
                                    @else
                                        <i class="far fa-square d-inline"></i>
                                        <p class="d-inline">12. Ganglios</p>
                                        <br>
                                    @endif
                                </div>
                            </div>

                        </div>


                        <div id="step-4">
                            <form class="form-horizontal form-label-left">
                                <br><br>

                                <div class="form-group row">
                                    <div class="col-md-12 col-sm-12 ">
                                        <div class="container">
                                            <div class="panel panel-primary overflow-auto">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Odontograma</h3>
                                                </div>
                                                <div id="nuevo" class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-6 offset-md-3">
                                                            <div id="controls" class="panel panel-default">
                                                                <div class="panel-body">
                                                                    <div class="btn-group" data-toggle="buttons">
                                                                        <label id="fractura" class="btn btn-danger active">
                                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked>Fractura</label>
                                                                        <label id="restauracion" class="btn btn-primary">
                                                                            <input type="radio" name="options" id="option2" autocomplete="off"> Obturación
                                                                        </label>
                                                                        <label id="extraccion" class="btn btn-warning">
                                                                            <input type="radio" name="options" id="option3" autocomplete="off"> Extracción
                                                                        </label>
                                                                        <label id="extraer" class="btn btn-warning">
                                                                            <input type="radio" name="options" id="option4" autocomplete="off"> A Extraer
                                                                        </label>
                                                                        <label id="endodoncia" class="btn btn-warning">
                                                                            <input type="radio" name="options" id="option7" autocomplete="off"> Endodoncia
                                                                        </label>
                                                                        <label id="puente" class="btn btn-primary">
                                                                            <input type="radio" name="options" id="option5" autocomplete="off"> Puente
                                                                        </label>
                                                                        <label id="borrar" class="btn btn-default">
                                                                            <input type="radio" name="options" id="option6" autocomplete="off"> Borrar
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="tr" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        </div>
                                                        <div id="tl" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        </div>
                                                        <div id="tlr" class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                                                        </div>
                                                        <div id="tll" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div id="blr" class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                                                        </div>
                                                        <div id="bll" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        </div>
                                                        <div id="br" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        </div>
                                                        <div id="bl" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="panel panel-default">
                                                                <div class="panel-body">
                                                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left">
                                                                        <div style="height: 20px; width:20px; display:inline-block;" class="click-red"></div> = Fractura/Carie
                                                                        <br>
                                                                        <div style="height: 5px; width:20px; display:inline-block;" class="click-red"></div> = Puente Entre Piezas
                                                                    </div>
                                                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center">
                                                                        <div style="height: 20px; width:20px; display:inline-block;" class="click-blue"></div> = Obturación
                                                                    </div>
                                                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                        <span style="display:inline:block;"> Pieza Extraída</span> = <img style="display:inline:block;" src="{{ url('img/extraccion.png') }}">
                                                                        <br> Idicada Para Extracción = <i style="color:red;" class="fa fa-times fa-2x"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>


                        <div id="step-5">

                            <form class="form-horizontal form-label-left">
                                <br><br>

                                <h5>Higiene oral simplificada</h5>

                                @php
                                 $piezas1_array = json_decode($detalle_paciente->piezas1);
                                 $piezas2_array = json_decode($detalle_paciente->piezas2);
                                 $piezas3_array = json_decode($detalle_paciente->piezas3);
                                 $piezas4_array = json_decode($detalle_paciente->piezas4);
                                 $piezas5_array = json_decode($detalle_paciente->piezas5);
                                 $piezas6_array = json_decode($detalle_paciente->piezas6);
                                @endphp

                                {{--7. Indicadores de salud Bucal--}}
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Piezas dentales</th>
                                        <th scope="col">Placa 0-1-2-3</th>
                                        <th scope="col">Cálculo 0-1-2-3</th>
                                        <th scope="col">Gingivitis 0-1</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{-- primera row--}}
                                    <tr>
                                        <td>
                                            @php
                                                $bandera1 = array(0,0,0);

                                                foreach ($piezas1_array as $pieza){
                                                    if($pieza == "16"){
                                                        $bandera1[0] = 1;
                                                    }

                                                    if($pieza == "27"){
                                                        $bandera1[1] = 1;
                                                    }

                                                    if($pieza == "55"){
                                                        $bandera1[2] = 1;
                                                    }
                                                }

                                            $bandera2 = array(0,0,0);

                                                foreach ($piezas2_array as $pieza){
                                                    if($pieza == "11"){
                                                        $bandera2[0] = 1;
                                                    }

                                                    if($pieza == "21"){
                                                        $bandera2[1] = 1;
                                                    }

                                                    if($pieza == "51"){
                                                        $bandera2[2] = 1;
                                                    }
                                                }

                                            $bandera3 = array(0,0,0);

                                                foreach ($piezas3_array as $pieza){
                                                    if($pieza == "26"){
                                                        $bandera3[0] = 1;
                                                    }

                                                    if($pieza == "27"){
                                                        $bandera3[1] = 1;
                                                    }

                                                    if($pieza == "65"){
                                                        $bandera3[2] = 1;
                                                    }
                                                }

                                            $bandera4 = array(0,0,0);

                                                foreach ($piezas4_array as $pieza){
                                                    if($pieza == "36"){
                                                        $bandera4[0] = 1;
                                                    }

                                                    if($pieza == "37"){
                                                        $bandera4[1] = 1;
                                                    }

                                                    if($pieza == "75"){
                                                        $bandera4[2] = 1;
                                                    }
                                                }

                                            $bandera5 = array(0,0,0);

                                                foreach ($piezas5_array as $pieza){
                                                    if($pieza == "31"){
                                                        $bandera5[0] = 1;
                                                    }

                                                    if($pieza == "41"){
                                                        $bandera5[1] = 1;
                                                    }

                                                    if($pieza == "72"){
                                                        $bandera5[2] = 1;
                                                    }
                                                }

                                            $bandera6 = array(0,0,0);

                                                foreach ($piezas6_array as $pieza){
                                                    if($pieza == "46"){
                                                        $bandera6[0] = 1;
                                                    }

                                                    if($pieza == "47"){
                                                        $bandera6[1] = 1;
                                                    }

                                                    if($pieza == "85"){
                                                        $bandera6[2] = 1;
                                                    }
                                                }

                                            @endphp


                                            {{--columna 1--}}
                                            @if ($bandera1[0]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">16</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">16</p>
                                            @endif

                                            @if ($bandera1[1]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">27</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">27</p>
                                            @endif

                                            @if ($bandera1[2]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">55</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">55</p>
                                            @endif

                                        </td>

                                        <td>
                                            {{ $detalle_paciente->placa1 }}
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->calculo1 }}
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->gingivitis1 }}
                                        </td>
                                    </tr>

                                    {{--segunda row--}}
                                    <tr>
                                        <td>
                                            {{--columna 2--}}
                                            @if ($bandera2[0]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">11</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">11</p>
                                            @endif

                                            @if ($bandera2[1]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">21</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">21</p>
                                            @endif

                                            @if ($bandera2[2]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">51</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">51</p>
                                            @endif
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->placa2 }}
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->calculo1 }}
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->gingivitis1 }}
                                        </td>
                                    </tr>

                                    {{--tercera row--}}
                                    <tr>
                                        <td>
                                            {{--columna 3--}}
                                            @if ($bandera3[0]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">26</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">26</p>
                                            @endif

                                            @if ($bandera3[1]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">27</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">27</p>
                                            @endif

                                            @if ($bandera3[2]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">65</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">65</p>
                                            @endif
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->placa3 }}
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->calculo3 }}
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->gingivitis3 }}
                                        </td>
                                    </tr>

                                    {{--cuarta row--}}
                                    <tr>
                                        <td>
                                            {{--columna 4--}}
                                            @if ($bandera4[0]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">36</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">36</p>
                                            @endif

                                            @if ($bandera4[1]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">37</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">37</p>
                                            @endif

                                            @if ($bandera4[2]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">75</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">75</p>
                                            @endif
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->placa4 }}
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->calculo4 }}
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->gingivitis4 }}
                                        </td>
                                    </tr>

                                    {{--quinta row--}}
                                    <tr>
                                        <td>
                                            {{--columna 5--}}
                                            @if ($bandera5[0]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">31</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">31</p>
                                            @endif

                                            @if ($bandera5[1]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">41</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">41</p>
                                            @endif

                                            @if ($bandera5[2]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">72</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">72</p>
                                            @endif
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->placa5 }}
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->calculo5 }}
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->gingivitis5 }}
                                        </td>
                                    </tr>

                                    {{--sexta row--}}
                                    <tr>
                                        <td>
                                            {{--columna 6--}}
                                            @if ($bandera6[0]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">46</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">46</p>
                                            @endif

                                            @if ($bandera6[1]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">47</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">47</p>
                                            @endif

                                            @if ($bandera6[2]  == "1")
                                                <i class="far fa-check-square d-inline"></i>
                                                <p class="d-inline">85</p>
                                            @else
                                                <i class="far fa-square d-inline"></i>
                                                <p class="d-inline">85</p>
                                            @endif
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->placa6 }}
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->calculo6 }}
                                        </td>

                                        <td>
                                            {{ $detalle_paciente->gingivitis6 }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Enfermedad periodontal</th>
                                        <th scope="col">Mal oclusión</th>
                                        <th scope="col">Fluorosis</th>
                                    <tbody>
                                    <tr>
                                        <td>
                                            @switch($detalle_paciente->periodontal)
                                                @case(0)
                                                <ul>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Leve</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Moderado</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Severa</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-check-square d-inline"></i>
                                                        <p class="d-inline">No tiene</p>
                                                    </li>
                                                </ul>
                                                @break

                                                @case(1)
                                                <ul>
                                                    <li>
                                                        <i class="far fa-check-square d-inline"></i>
                                                        <p class="d-inline">Leve</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Moderado</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Severa</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">No tiene</p>
                                                    </li>
                                                </ul>
                                                @break

                                                @case(2)
                                            <ul>
                                                <li>
                                                    <i class="far fa-square d-inline"></i>
                                                    <p class="d-inline">Leve</p>
                                                </li>
                                                <li>
                                                    <i class="far fa-check-square d-inline"></i>
                                                    <p class="d-inline">Moderado</p>
                                                </li>
                                                <li>
                                                    <i class="far fa-square d-inline"></i>
                                                    <p class="d-inline">Severa</p>
                                                </li>
                                                <li>
                                                    <i class="far fa-square d-inline"></i>
                                                    <p class="d-inline">No tiene</p>
                                                </li>
                                            </ul>
                                                @break

                                                @case(3)
                                                <ul>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Leve</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Moderado</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-check-square d-inline"></i>
                                                        <p class="d-inline">Severa</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">No tiene</p>
                                                    </li>
                                                </ul>
                                                @break

                                                @default
                                                <ul>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Leve</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Moderado</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Severa</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">No tiene</p>
                                                    </li>
                                                </ul>
                                            @endswitch

                                        </td>

                                        <td>
                                            @switch($detalle_paciente->oclusion)
                                                @case(0)
                                                <ul>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Angle I</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Angle II</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Angle III</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-check-square d-inline"></i>
                                                        <p class="d-inline">No tiene</p>
                                                    </li>
                                                </ul>
                                                @break

                                                @case(1)
                                                <ul>
                                                    <li>
                                                        <i class="far fa-check-square d-inline"></i>
                                                        <p class="d-inline">Angle I</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Angle II</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Angle III</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">No tiene</p>
                                                    </li>
                                                </ul>
                                                @break

                                                @case(2)
                                                <ul>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">AngleI</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-check-square d-inline"></i>
                                                        <p class="d-inline">Angle II</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Angle III</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">No tiene</p>
                                                    </li>
                                                </ul>
                                                @break

                                                @case(3)
                                                <ul>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">AngleI</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Angle II</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-check-square d-inline"></i>
                                                        <p class="d-inline">Angle III</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">No tiene</p>
                                                    </li>
                                                </ul>
                                                @break

                                                @default
                                                <ul>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">AngleI</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Angle II</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Angle III</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">No tiene</p>
                                                    </li>
                                                </ul>
                                            @endswitch
                                        </td>

                                        <td>
                                            @switch($detalle_paciente->fluorosis)
                                                @case(0)
                                                <ul>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Leve</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Moderado</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Severa</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-check-square d-inline"></i>
                                                        <p class="d-inline">No tiene</p>
                                                    </li>
                                                </ul>
                                                @break

                                                @case(1)
                                                <ul>
                                                    <li>
                                                        <i class="far fa-check-square d-inline"></i>
                                                        <p class="d-inline">Leve</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Moderado</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Severa</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">No tiene</p>
                                                    </li>
                                                </ul>
                                                @break

                                                @case(2)
                                                <ul>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Leve</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-check-square d-inline"></i>
                                                        <p class="d-inline">Moderado</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Severa</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">No tiene</p>
                                                    </li>
                                                </ul>
                                                @break

                                                @case(3)
                                                <ul>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Leve</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Moderado</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-check-square d-inline"></i>
                                                        <p class="d-inline">Severa</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">No tiene</p>
                                                    </li>
                                                </ul>
                                                @break

                                                @default
                                                <ul>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Leve</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Moderado</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">Severa</p>
                                                    </li>
                                                    <li>
                                                        <i class="far fa-square d-inline"></i>
                                                        <p class="d-inline">No tiene</p>
                                                    </li>
                                                </ul>
                                            @endswitch
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>

                        <div id="step-6">
                            <form class="form-horizontal form-label-left">
                                <br><br>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">D</th>
                                        <th scope="col">C</th>
                                        <th scope="col">P</th>
                                        <th scope="col">O</th>
                                    <tbody>
                                    <tr>
                                        <td>

                                        </td>

                                        <td>
                                            {{$detalle_paciente->indice_c}}
                                        </td>

                                        <td>
                                            {{$detalle_paciente->indice_p}}
                                        </td>

                                        <td>
                                            {{$detalle_paciente->indice_o}}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">d</th>
                                        <th scope="col">c</th>
                                        <th scope="col">e</th>
                                        <th scope="col">o</th>
                                    <tbody>
                                    <tr>
                                        <td>

                                        </td>

                                        <td>
                                            {{$detalle_paciente->indice_c2}}
                                        </td>

                                        <td>
                                            {{$detalle_paciente->indice_e2}}
                                        </td>

                                        <td>
                                            {{$detalle_paciente->indice_o2}}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            </form>
                        </div>



                    </div>
                    <!-- End SmartWizard Content -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<!-- jQuery Smart Wizard -->
<script src="{{ asset('js/wizard_primera_cita.js') }}"></script>
<script>
    function replaceAll(find, replace, str) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    function createOdontogram() {
        var htmlLecheLeft = "",
            htmlLecheRight = "",
            htmlLeft = "",
            htmlRight = "",
            a = 1;
        for (var i = 9 - 1; i >= 1; i--) {
            //Dientes Definitivos Cuandrante Derecho (Superior/Inferior)
            htmlRight += '<div data-name="value" id="dienteindex' + i + '" class="diente">' +
                '<span style="margin-left: 45px; margin-bottom:5px; display: inline-block !important; border-radius: 10px !important;" class="label label-info">index' + i + '</span>' +
                '<div id="tindex' + i + '" class="cuadro click">' +
                '</div>' +
                '<div id="lindex' + i + '" class="cuadro izquierdo click">' +
                '</div>' +
                '<div id="bindex' + i + '" class="cuadro debajo click">' +
                '</div>' +
                '<div id="rindex' + i + '" class="cuadro derecha click click">' +
                '</div>' +
                '<div id="cindex' + i + '" class="centro click">' +
                '</div>' +
                '</div>';
            //Dientes Definitivos Cuandrante Izquierdo (Superior/Inferior)
            htmlLeft += '<div id="dienteindex' + a + '" class="diente">' +
                '<span style="margin-left: 45px; margin-bottom:5px; display: inline-block !important; border-radius: 10px !important;" class="label label-info">index' + a + '</span>' +
                '<div id="tindex' + a + '" class="cuadro click">' +
                '</div>' +
                '<div id="lindex' + a + '" class="cuadro izquierdo click">' +
                '</div>' +
                '<div id="bindex' + a + '" class="cuadro debajo click">' +
                '</div>' +
                '<div id="rindex' + a + '" class="cuadro derecha click click">' +
                '</div>' +
                '<div id="cindex' + a + '" class="centro click">' +
                '</div>' +
                '</div>';
            if (i <= 5) {
                //Dientes Temporales Cuandrante Derecho (Superior/Inferior)
                htmlLecheRight += '<div id="dienteLindex' + i + '" style="left: -25%;" class="diente-leche">' +
                    '<span style="margin-left: 45px; margin-bottom:5px; display: inline-block !important; border-radius: 10px !important;" class="label label-primary">index' + i + '</span>' +
                    '<div id="tlecheindex' + i + '" class="cuadro-leche top-leche click">' +
                    '</div>' +
                    '<div id="llecheindex' + i + '" class="cuadro-leche izquierdo-leche click">' +
                    '</div>' +
                    '<div id="blecheindex' + i + '" class="cuadro-leche debajo-leche click">' +
                    '</div>' +
                    '<div id="rlecheindex' + i + '" class="cuadro-leche derecha-leche click click">' +
                    '</div>' +
                    '<div id="clecheindex' + i + '" class="centro-leche click">' +
                    '</div>' +
                    '</div>';
            }
            if (a < 6) {
                //Dientes Temporales Cuandrante Izquierdo (Superior/Inferior)
                htmlLecheLeft += '<div id="dienteLindex' + a + '" class="diente-leche">' +
                    '<span style="margin-left: 45px; margin-bottom:5px; display: inline-block !important; border-radius: 10px !important;" class="label label-primary">index' + a + '</span>' +
                    '<div id="tlecheindex' + a + '" class="cuadro-leche top-leche click">' +
                    '</div>' +
                    '<div id="llecheindex' + a + '" class="cuadro-leche izquierdo-leche click">' +
                    '</div>' +
                    '<div id="blecheindex' + a + '" class="cuadro-leche debajo-leche click">' +
                    '</div>' +
                    '<div id="rlecheindex' + a + '" class="cuadro-leche derecha-leche click click">' +
                    '</div>' +
                    '<div id="clecheindex' + a + '" class="centro-leche click">' +
                    '</div>' +
                    '</div>';
            }
            a++;
        }
        $("#tr").append(replaceAll('index', '1', htmlRight));
        $("#tl").append(replaceAll('index', '2', htmlLeft));
        $("#tlr").append(replaceAll('index', '5', htmlLecheRight));
        $("#tll").append(replaceAll('index', '6', htmlLecheLeft));


        $("#bl").append(replaceAll('index', '3', htmlLeft));
        $("#br").append(replaceAll('index', '4', htmlRight));
        $("#bll").append(replaceAll('index', '7', htmlLecheLeft));
        $("#blr").append(replaceAll('index', '8', htmlLecheRight));
    }
    var arrayPuente = [];
    var arrayPiezas = [];

    function eliminar_duplicados(id, diente, tratamiento) {
        console.log('tamano: ', arrayPiezas.length);
        var contador = 0;
        if (arrayPiezas.length == 0) {
            arrayPiezas.push({
                id: id,
                diente: diente.substr(0),
                tratamiento: tratamiento
            });
        } else {
            arrayPiezas.forEach(element => {
                console.log('contador', contador);
                console.log('duplicado', element);
                console.log('duplicado2 id', id);
                console.log('duplicado2 tratamiento', tratamiento);
                if (element.id != id || tratamiento != element.tratamiento) {
                    if (contador == 0) {
                        arrayPiezas.push({
                            id: id,
                            diente: diente.substr(0),
                            tratamiento: tratamiento
                        });
                    }

                    contador++;
                }
            });
        }
        console.log('arrayPiezas: ', arrayPiezas);
    }

    function desmarcar(id, diente, tratamiento) {
        arrayPiezas.forEach((element, index) => {
            console.log('desmarcar', id, diente, tratamiento);
            if (element.id == id && tratamiento == element.tratamiento) {
                arrayPiezas.splice(index, 1)
            }
        });
    }




    $(document).ready(function() {
        createOdontogram();
        $("#nuevo").click(function() {
            console.log(arrayPiezas);
        });

        var id =  $( "#my_id" ).val();

        function validacion(){
            var arrErrores = [];

            if( $("#problema_actual").val().length > 255 ) {
                arrErrores.push($("#problema_actual").attr("name"));
            }

            if( $("#patologia_text").val().length > 255 ) {
                arrErrores.push($("#patologia_text").attr("name"));
            }

            return arrErrores;
        }

        $("#guardarfinal").click(function() {
            var arrErr = validacion();

            if( arrErr.length == 0) {

                ArrPiezaFila1 = [];
                $("input:checkbox[name=muela_fila1]:checked").each(function () {
                    ArrPiezaFila1.push($(this).val());
                });
                ArrPiezaFila1 = (ArrPiezaFila1.length == 0) ? [0] : ArrPiezaFila1;
                placa_fila1 = $("input[name=placa_fila1]").val();
                calculo_fila1 = $("input[name=calculo_fila1]").val();
                gingivitis_fila1 = $("input[name=gingivitis_fila1]").val();

                ArrPiezaFila2 = [];
                $("input:checkbox[name=muela_fila2]:checked").each(function () {
                    ArrPiezaFila2.push($(this).val());
                });
                ArrPiezaFila2 = (ArrPiezaFila2.length == 0) ? [0] : ArrPiezaFila2;
                placa_fila2 = $("input[name=placa_fila2]").val();
                calculo_fila2 = $("input[name=calculo_fila2]").val();
                gingivitis_fila2 = $("input[name=gingivitis_fila2]").val();

                ArrPiezaFila3 = [];
                $("input:checkbox[name=muela_fila3]:checked").each(function () {
                    ArrPiezaFila3.push($(this).val());
                });
                ArrPiezaFila3 = (ArrPiezaFila3.length == 0) ? [0] : ArrPiezaFila3;
                placa_fila3 = $("input[name=placa_fila3]").val();
                calculo_fila3 = $("input[name=calculo_fila3]").val();
                gingivitis_fila3 = $("input[name=gingivitis_fila3]").val();

                ArrPiezaFila4 = [];
                $("input:checkbox[name=muela_fila4]:checked").each(function () {
                    ArrPiezaFila4.push($(this).val());
                });
                ArrPiezaFila4 = (ArrPiezaFila4.length == 0) ? [0] : ArrPiezaFila4;
                placa_fila4 = $("input[name=placa_fila4]").val();
                calculo_fila4 = $("input[name=calculo_fila4]").val();
                gingivitis_fila4 = $("input[name=gingivitis_fila4]").val();

                ArrPiezaFila5 = [];
                $("input:checkbox[name=muela_fila5]:checked").each(function () {
                    ArrPiezaFila5.push($(this).val());
                });
                ArrPiezaFila5 = (ArrPiezaFila5.length == 0) ? [0] : ArrPiezaFila5;
                placa_fila5 = $("input[name=placa_fila5]").val();
                calculo_fila5 = $("input[name=calculo_fila5]").val();
                gingivitis_fila5 = $("input[name=gingivitis_fila5]").val();

                ArrPiezaFila6 = [];
                $("input:checkbox[name=muela_fila6]:checked").each(function () {
                    ArrPiezaFila6.push($(this).val());
                });
                ArrPiezaFila6 = (ArrPiezaFila6.length == 0) ? [0] : ArrPiezaFila6;
                placa_fila6 = $("input[name=placa_fila6]").val();
                calculo_fila6 = $("input[name=calculo_fila6]").val();
                gingivitis_fila6 = $("input[name=gingivitis_fila6]").val();

                enfermedad_periodontal = $("input[name=enfermedad_periodontal]:checked").val();
                enfermedad_periodontal = (enfermedad_periodontal == null) ? 0 : enfermedad_periodontal;
                mal_oclusion = $("input[name=mal_oclusion]:checked").val();
                mal_oclusion = (mal_oclusion == null) ? 0 : mal_oclusion;
                fluorosis = $("input[name=fluorosis]:checked").val();
                fluorosis = (fluorosis == null) ? 0 : fluorosis;

                indice_c = $("input[name=indice_c]").val();
                indice_p = $("input[name=indice_p]").val();
                indice_o = $("input[name=indice_o]").val();
                indice_c2 = $("input[name=indice_c2]").val();
                indice_e2 = $("input[name=indice_e2]").val();
                indice_o2 = $("input[name=indice_o2]").val();


                $.ajax({
                    url: '/detalle-paciente/' + id,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        problema_actual: problema_actual,
                        presion_arterial: presion_arterial,
                        fr_cardiaca: fr_cardiaca,
                        temperatura: temperatura,
                        fr_respiratoria: fr_respiratoria,

                        labios: labios,
                        mejillas: mejillas,
                        maxilar_superior: maxilar_superior,
                        maxilar_inferior: maxilar_inferior,
                        lengua: lengua,
                        paladar: paladar,
                        piso: piso,
                        carrillos: carrillos,
                        glandulas_salivales: glandulas_salivales,
                        faringe: faringe,
                        atm: atm,
                        ganglios: ganglios,
                        patologia_text: patologia_text,

                        muela_fila1: ArrPiezaFila1,
                        placa_fila1: placa_fila1,
                        calculo_fila1: calculo_fila1,
                        gingivitis_fila1: gingivitis_fila1,

                        muela_fila2: ArrPiezaFila2,
                        placa_fila2: placa_fila2,
                        calculo_fila2: calculo_fila2,
                        gingivitis_fila2: gingivitis_fila2,

                        muela_fila3: ArrPiezaFila3,
                        placa_fila3: placa_fila3,
                        calculo_fila3: calculo_fila3,
                        gingivitis_fila3: gingivitis_fila3,

                        muela_fila4: ArrPiezaFila4,
                        placa_fila4: placa_fila4,
                        calculo_fila4: calculo_fila4,
                        gingivitis_fila4: gingivitis_fila4,

                        muela_fila5: ArrPiezaFila5,
                        placa_fila5: placa_fila5,
                        calculo_fila5: calculo_fila5,
                        gingivitis_fila5: gingivitis_fila5,

                        muela_fila6: ArrPiezaFila6,
                        placa_fila6: placa_fila6,
                        calculo_fila6: calculo_fila6,
                        gingivitis_fila6: gingivitis_fila6,

                        enfermedad_periodontal: enfermedad_periodontal,
                        mal_oclusion: mal_oclusion,
                        fluorosis: fluorosis,

                        indice_c: indice_c,
                        indice_p: indice_p,
                        indice_o: indice_o,
                        indice_c2: indice_c2,
                        indice_e2: indice_e2,
                        indice_o2: indice_o2,

                        arrayPiezas: arrayPiezas,
                        arrayPuente: arrayPuente,

                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'PUT',
                    dataType: 'json',
                    success: function () {
                        Swal.fire(
                            'Bien!',
                            'historia clínica guardada con éxito',
                            'success'
                        )

                    },
                    error: function (xhr, status) {
                        Swal.fire({
                            icon: 'warning',
                            title: 'Oops...',
                            text: 'Disculpe, existió un problema',
                        })
                    },
                    /* complete: function(xhr, status) {
                        alert('Petición realizada');
                    } */
                });

            }else{

                arrErr.forEach(function(err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: err+' no debe tener mas de 250 caracteres',
                    })

                });
            }

        });


        $(".click").click(function(event) {
            var control = $("#controls").children().find('.active').attr('id');
            var cuadro = $(this).find("input[name=cuadro]:hidden").val();
            console.log($(this).attr('id'))
            switch (control) {
                case "fractura":
                    if ($(this).hasClass("click-blue")) {
                        $(this).removeClass('click-blue');
                        $(this).addClass('click-red');
                        desmarcar($(this).attr('id'), $(this).attr('id').substr(1), 'restauracion');
                        eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'fractura');
                    } else {
                        if ($(this).hasClass("click-red")) {
                            $(this).removeClass('click-red');
                            desmarcar($(this).attr('id'), $(this).attr('id').substr(1), 'fractura');
                        } else {
                            $(this).addClass('click-red');
                            eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'fractura');
                        }
                    }
                    break;
                case "restauracion":
                    if ($(this).hasClass("click-red")) {
                        $(this).removeClass('click-red');
                        $(this).addClass('click-blue');
                        desmarcar($(this).attr('id'), $(this).attr('id').substr(1), 'fractura');
                        eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'restauracion');
                    } else {
                        if ($(this).hasClass("click-blue")) {
                            $(this).removeClass('click-blue');
                            desmarcar($(this).attr('id'), $(this).attr('id').substr(1), 'restauracion');
                        } else {
                            $(this).addClass('click-blue');
                            eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'restauracion');
                        }
                    }
                    break;
                case "extraccion":
                    var dientePosition = $(this).position();
                    console.log('this: ', $(this))
                    console.log('dienteposition', dientePosition)
                    $(this).parent().children().each(function(index, el) {
                        if ($(el).hasClass("click-delete")) {
                            $(this).removeClass('click-delete');
                            console.log('entro en delete');
                            if (index == 4) {
                                desmarcar($(this).attr('id'), $(this).attr('id').substr(1), 'extraida');
                            }

                        } else if ($(el).hasClass("click")) {
                            console.log('entro en click');
                            $(el).addClass('click-delete');
                            if (index == 4) {
                                eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'extraida');
                            }
                        }
                    });
                    break;
                case "extraer":
                    var dientePosition = $(this).position();

                    $(this).parent().children().each(function(index, el) {
                        console.log('------');
                        if ($(el).hasClass("centro") || $(el).hasClass("centro-leche")) {
                            $(this).parent().append('<i id="extraccion_img" style="color:red;" class="fa fa-times fa-3x fa-fw"></i>');
                            if ($(el).hasClass("centro")) {
                                //console.log($(this).parent().children("i"))
                                $(this).parent().children("i").css({
                                    "position": "absolute",
                                    "top": "24%",
                                    "width": "115"
                                });
                                eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'extraer');
                            } else {
                                $(this).parent().children("i").css({
                                    "position": "absolute",
                                    "top": "21%",
                                    "left": "1.2ex"
                                });
                                eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'extraer');
                            }
                            //
                        }
                    });
                    break;
                case "endodoncia":
                    var dientePosition = $(this).position();
                    //console.log($(this))
                    //console.log(dientePosition)
                    //console.log($(this).parent().children());
                    $(this).parent().children().each(function(index, el) {
                        console.log('------');
                        if ($(el).hasClass("centro") || $(el).hasClass("centro-leche")) {
                            $(this).parent().append('<i id="endodoncia_img" style="color:blue;" class="fa fa-caret-up fa-3x fa-fw"></i>');
                            if ($(el).hasClass("centro")) {
                                //console.log($(this).parent().children("i"))
                                $(this).parent().children("i").css({
                                    "position": "absolute",
                                    "top": "24%",
                                    "width": "115"
                                });
                                eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'endodoncia');
                            } else {
                                $(this).parent().children("i").css({
                                    "position": "absolute",
                                    "top": "21%",
                                    "left": "1.2ex"
                                });
                                eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'endodoncia');
                            }
                            //
                        }
                    });
                    break;
                case "puente":
                    var dientePosition = $(this).offset(),
                        leftPX;
                    console.log($(this)[0].offsetLeft)
                    var noDiente = $(this).parent().children().first().text();
                    var cuadrante = $(this).parent().parent().attr('id');
                    var left = 0,
                        width = 0;
                    if (arrayPuente.length < 1) {
                        $(this).parent().children('.cuadro').css('border-color', 'red');
                        arrayPuente.push({
                            diente: noDiente,
                            cuadrante: cuadrante,
                            left: $(this)[0].offsetLeft,
                            father: null
                        });
                    } else {
                        $(this).parent().children('.cuadro').css('border-color', 'red');
                        arrayPuente.push({
                            diente: noDiente,
                            cuadrante: cuadrante,
                            left: $(this)[0].offsetLeft,
                            father: arrayPuente[0].diente
                        });
                        var diferencia = Math.abs((parseInt(arrayPuente[1].diente) - parseInt(arrayPuente[1].father)));
                        if (diferencia == 1) width = diferencia * 60;
                        else width = diferencia * 50;

                        if (arrayPuente[0].cuadrante == arrayPuente[1].cuadrante) {
                            if (arrayPuente[0].cuadrante == 'tr' || arrayPuente[0].cuadrante == 'tlr' || arrayPuente[0].cuadrante == 'br' || arrayPuente[0].cuadrante == 'blr') {
                                if (arrayPuente[0].diente > arrayPuente[1].diente) {
                                    console.log(arrayPuente[0])
                                    leftPX = (parseInt(arrayPuente[0].left) + 10) + "px";
                                } else {
                                    leftPX = (parseInt(arrayPuente[1].left) + 10) + "px";
                                    //leftPX = "45px";
                                }
                            } else {
                                if (arrayPuente[0].diente < arrayPuente[1].diente) {
                                    leftPX = "-45px";
                                } else {
                                    leftPX = "45px";
                                }
                            }
                        }
                        console.log(leftPX);
                        /*$(this).parent().append('<div style="z-index: 9999; height: 5px; width:' + width + 'px;" id="puente" class="click-red"></div>');
                        $(this).parent().children().last().css({
                            "position": "absolute",
                            "top": "80px",
                            "left": "50px"
                        });*/
                        $(this).parent().append('<div style="z-index: 9999; height: 5px; width:' + width + 'px;" id="puente" class="click-red"></div>');
                        $(this).parent().children().last().css({
                            "position": "absolute",
                            "top": "80px",
                            "left": leftPX
                        });
                    }

                    break;
                default:
                    console.log("borrar case");
                    $('#extraccion_img').remove();
                    $('#endodoncia_img').remove();
                    desmarcar('c' + $(this).attr('id').substr(1), $(this).attr('id').substr(1), 'endodoncia');
                    desmarcar('c' + $(this).attr('id').substr(1), $(this).attr('id').substr(1), 'extraer');

            }
            return false;
        });
        return false;
    });
</script>
@endsection
