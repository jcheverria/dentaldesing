@extends('base')

@section('title', 'nuevo2')

@section('estilo')
    <link href="{{ asset('css/historia-style.css') }}" rel="stylesheet">
@endsection

@section('content')


    <h4> paciente: {{$paciente->primer_nombre}} {{$paciente->primer_apellido}}</h4>
    <h6> su motivo de consulta es: {{$detalle_paciente->motivo_consulta}}</h6>
    <h6>el paciente loggueado es {{ Auth::user() }}</h6>

<div>
    <div class="row">
        @if ($errors->any())

            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li> {{ $error }} </li>
                    @endforeach
                </ul>
            </div>
            </ul>
        @endif
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Historia clínica</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <!-- Smart Wizard -->
                    <p>En el siguiente formulario se tomara información fundamental del paciente.</p>
                    <div id="wizard" class="form_wizard wizard_horizontal">
                        <ul class="wizard_steps">
                            <li>
                                <a href="#step-1">
                                    <span class="step_no">1</span>
                                    <span class="step_descr">
                                        Información de consulta
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-2">
                                    <span class="step_no">2</span>
                                    <span class="step_descr">
                                        Signos vitales
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-3">
                                    <span class="step_no">3</span>
                                    <span class="step_descr">
                                        Examen del sistema estomatognático
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-4">
                                    <span class="step_no">4</span>
                                    <span class="step_descr">
                                        Odontograma
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-5">
                                    <span class="step_no">5</span>
                                    <span class="step_descr">
                                        Indicadores de salud bucal
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#step-6">
                                    <span class="step_no">6</span>
                                    <span class="step_descr">
                                        índices CPO-ceo
                                    </span>
                                </a>
                            </li>

                        </ul>

                        <div id="step-1">
                            <form class="form-horizontal form-label-left">
                                <div class="form-group">
                                    <input class="form-check-input" type="hidden" value="{{$paciente->id}}" id="my_id" name="my_id"> {{--campo oculto para tener el id--}}
                                    <label>Problema dental actual:</label>
                                    <textarea id="problema_actual" name="Problema dental actual" class="form-control" rows="3">{{ old('problema_actual', $detalle_paciente->problema_actual) }}</textarea>
                                </div>
                            </form>
                        </div>

                        <div id="step-2">
                            <form class="form-horizontal form-label-left">
                                <br><br>
                                <div class="form-inline">
                                    <label for="presion_arterial">Presión arterial</label>
                                    <input type="text" name="presion_arterial" class="form-control mb-2 mr-sm-2"
                                           value=" {{ old('presion_arterial' , $detalle_paciente->presion_arterial) }}">


                                    <label for="fr_cardiaca">Frecuencia Cardiaca</label>
                                    <input type="text" name="fr_cardiaca" class="form-control mb-2 mr-sm-2"
                                           value=" {{ old('fr_cardiaca', $detalle_paciente->frecuencia_cardiaca) }}">
                                </div>


                                <div class="form-inline">
                                    <label for="temperatura">Temperatura</label>
                                    <input type="text" name="temperatura" class="form-control mb-2 mr-sm-2"
                                           value=" {{ old('temperatura', $detalle_paciente->temperatura) }}">
                                    <label for="fr_respiratoria">F. Respiratoria min.</label>
                                    <input type="text" name="fr_respiratoria" class="form-control mb-2 mr-sm-2"
                                           value=" {{ old('fr_respiratoria',$detalle_paciente->frecuencia_respiratoria) }}">
                                </div>
                            </form>
                        </div>

                        <div id="step-3">
                            <form class="form-horizontal form-label-left">
                                <div class="form-group">
                                    <label>Examen del sistema estomatognático</label>

                                    <div class="form-check">

                                        <input class="form-check-input" type="checkbox"  id="labios" name="labios">
                                        <label class="form-check-label" for="labios">
                                            1. Labios
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"  id="mejillas" name="mejillas">
                                        <label class="form-check-label" for="mejillas">
                                            2. Mejillas
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"  id="maxilar_superior"
                                               name="maxilar_superior">
                                        <label class="form-check-label" for="maxilar_superior">
                                            3. Maxilar superior
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"  id="maxilar_inferior"
                                               name="maxilar_inferior">
                                        <label class="form-check-label" for="maxilar_inferior">
                                            4. Maxilar inferior
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"  id="lengua" name="lengua">
                                        <label class="form-check-label" for="lengua">
                                            5. Lengua
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"  id="paladar" name="paladar">
                                        <label class="form-check-label" for="paladar">
                                            6. Paladar
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"  id="piso" name="piso">
                                        <label class="form-check-label" for="piso">
                                            7. Piso
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"  id="carrillos" name="carrillos">
                                        <label class="form-check-label" for="carrillos">
                                            8. Carrillos
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"  id="glandulas_salivales"
                                               name="glandulas_salivales">
                                        <label class="form-check-label" for="glandulas_salivales">
                                            9. Glándulas salivales
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"  id="faringe" name="faringe">
                                        <label class="form-check-label" for="faringe">
                                            10. Oro Faringe
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"  id="atm" name="atm">
                                        <label class="form-check-label" for="atm">
                                            11. A.T.M
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"  id="ganglios" name="ganglios">
                                        <label class="form-check-label" for="ganglios">
                                            12. Ganglios
                                        </label>
                                    </div>

                                </div>

                                <textarea name="Descripción del examen estomatognático"
                                          id="patologia_text"
                                          class="form-control"
                                          rows="3"
                                          placeholder="Describa la patología de la región afectada">{{ old('patologia_text') }}</textarea>
                            </form>
                        </div>


                        <div id="step-4">
                            <form class="form-horizontal form-label-left">
                                <br><br>

                                <div class="form-group row">
                                    <div class="col-md-12 col-sm-12 ">
                                        <div class="container">
                                            <div class="panel panel-primary overflow-auto">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Odontograma</h3>
                                                </div>
                                                <div id="nuevo" class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-6 offset-md-3">
                                                            <div id="controls" class="panel panel-default">
                                                                <div class="panel-body">
                                                                    <div class="btn-group" data-toggle="buttons">
                                                                        <label id="fractura" class="btn btn-danger active">
                                                                            <input type="radio" name="options" id="option1" autocomplete="off" checked>Fractura</label>
                                                                        <label id="restauracion" class="btn btn-primary">
                                                                            <input type="radio" name="options" id="option2" autocomplete="off"> Obturación
                                                                        </label>
                                                                        <label id="extraccion" class="btn btn-warning">
                                                                            <input type="radio" name="options" id="option3" autocomplete="off"> Extracción
                                                                        </label>
                                                                        <label id="extraer" class="btn btn-warning">
                                                                            <input type="radio" name="options" id="option4" autocomplete="off"> A Extraer
                                                                        </label>
                                                                        <label id="endodoncia" class="btn btn-warning">
                                                                            <input type="radio" name="options" id="option7" autocomplete="off"> Endodoncia
                                                                        </label>
                                                                        <label id="puente" class="btn btn-primary">
                                                                            <input type="radio" name="options" id="option5" autocomplete="off"> Puente
                                                                        </label>
                                                                        <label id="borrar" class="btn btn-default">
                                                                            <input type="radio" name="options" id="option6" autocomplete="off"> Borrar
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="tr" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        </div>
                                                        <div id="tl" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        </div>
                                                        <div id="tlr" class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                                                        </div>
                                                        <div id="tll" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div id="blr" class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                                                        </div>
                                                        <div id="bll" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        </div>
                                                        <div id="br" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        </div>
                                                        <div id="bl" class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="panel panel-default">
                                                                <div class="panel-body">
                                                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left">
                                                                        <div style="height: 20px; width:20px; display:inline-block;" class="click-red"></div> = Fractura/Carie
                                                                        <br>
                                                                        <div style="height: 5px; width:20px; display:inline-block;" class="click-red"></div> = Puente Entre Piezas
                                                                    </div>
                                                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center">
                                                                        <div style="height: 20px; width:20px; display:inline-block;" class="click-blue"></div> = Obturación
                                                                    </div>
                                                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right">
                                                                        <span style="display:inline:block;"> Pieza Extraída</span> = <img style="display:inline:block;" src="{{ url('img/extraccion.png') }}">
                                                                        <br> Idicada Para Extracción = <i style="color:red;" class="fa fa-times fa-2x"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>


                        <div id="step-5">
                            <form class="form-horizontal form-label-left">
                                <br><br>

                                <h5>Higiene oral simplificada</h5>

                                {{--7. Indicadores de salud Bucal--}}
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Piezas dentales</th>
                                        <th scope="col">Placa 0-1-2-3</th>
                                        <th scope="col">Cálculo 0-1-2-3</th>
                                        <th scope="col">Gingivitis 0-1</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{-- primera row--}}
                                    <tr>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila1" id="inlineRadio1" value="16">
                                                <label class="form-check-label" for="inlineRadio1">16</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila1" id="inlineRadio2" value="27">
                                                <label class="form-check-label" for="inlineRadio2">27</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila1" id="inlineRadio3" value="55">
                                                <label class="form-check-label" for="inlineRadio3">55</label>
                                            </div>
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="3" name="placa_fila1" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('placa_fila1') }}">
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="3" name="calculo_fila1" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('calculo_fila1') }}">
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="1" name="gingivitis_fila1" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('gingivitis_fila1') }}">
                                        </td>
                                    </tr>

                                    {{--segunda row--}}
                                    <tr>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila2" id="inlineRadio4" value="11">
                                                <label class="form-check-label" for="inlineRadio4">11</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila2" id="inlineRadio5" value="21">
                                                <label class="form-check-label" for="inlineRadio5">21</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila2" id="inlineRadio6" value="51">
                                                <label class="form-check-label" for="inlineRadio6">51</label>
                                            </div>
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="3" name="placa_fila2" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('placa_fila2') }}">
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="3" name="calculo_fila2" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('calculo_fila2') }}">
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="1" name="gingivitis_fila2" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('gingivitis_fila2') }}">
                                        </td>
                                    </tr>

                                    {{--tercera row--}}
                                    <tr>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila3" id="inlineRadio7" value="26">
                                                <label class="form-check-label" for="inlineRadio7">26</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila3" id="inlineRadio8" value="27">
                                                <label class="form-check-label" for="inlineRadio8">27</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila3" id="inlineRadio9" value="65">
                                                <label class="form-check-label" for="inlineRadio9">65</label>
                                            </div>
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="3" name="placa_fila3" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('placa_fila3') }}">
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="3" name="calculo_fila3" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('calculo_fila3') }}">
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="1" name="gingivitis_fila3" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('gingivitis_fila3') }}">
                                        </td>
                                    </tr>

                                    {{--cuarta row--}}
                                    <tr>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila4" id="inlineRadio10" value="36">
                                                <label class="form-check-label" for="inlineRadio10">36</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila4" id="inlineRadio11" value="37">
                                                <label class="form-check-label" for="inlineRadio11">37</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila4" id="inlineRadio12" value="75">
                                                <label class="form-check-label" for="inlineRadio12">75</label>
                                            </div>
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="3" name="placa_fila4" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('placa_fila4') }}">
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="3" name="calculo_fila4" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('calculo_fila4') }}">
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="1" name="gingivitis_fila4" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('gingivitis_fila4') }}">
                                        </td>
                                    </tr>

                                    {{--quinta row--}}
                                    <tr>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila5" id="inlineRadio13" value="31">
                                                <label class="form-check-label" for="inlineRadio13">31</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila5" id="inlineRadio14" value="41">
                                                <label class="form-check-label" for="inlineRadio14">41</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila5" id="inlineRadio15" value="71">
                                                <label class="form-check-label" for="inlineRadio15">72</label>
                                            </div>
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="3" name="placa_fila5" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('placa_fila5') }}">
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="3" name="calculo_fila5" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('calculo_fila5') }}">
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="1" name="gingivitis_fila5" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('gingivitis_fila5') }}">
                                        </td>
                                    </tr>

                                    {{--sexta row--}}
                                    <tr>
                                        <td>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila6" id="inlineRadio16" value="46">
                                                <label class="form-check-label" for="inlineRadio16">46</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila6" id="inlineRadio17" value="47">
                                                <label class="form-check-label" for="inlineRadio17">47</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="muela_fila6" id="inlineRadio18" value="85">
                                                <label class="form-check-label" for="inlineRadio18">85</label>
                                            </div>
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="3" name="placa_fila6" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('placa_fila6') }}">
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="3" name="calculo_fila6" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('calculo_fila6') }}">
                                        </td>

                                        <td>
                                            <input type="number" min="0" max="1" name="gingivitis_fila6" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('gingivitis_fila6') }}">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Enfermedad periodontal</th>
                                        <th scope="col">Mal oclusión</th>
                                        <th scope="col">Fluorosis</th>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="enfermedad_periodontal" id="leve"
                                                       value="1">
                                                <label class="form-check-label" for="leve">
                                                    Leve
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="enfermedad_periodontal" id="moderada"
                                                       value="2">
                                                <label class="form-check-label" for="moderada">
                                                    Moderada
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="enfermedad_periodontal" id="severa"
                                                       value="3">
                                                <label class="form-check-label" for="severa">
                                                    Severa
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="enfermedad_periodontal" id="ep_no_tiene"
                                                       value="0">
                                                <label class="form-check-label" for="ep_no_tiene">
                                                    No tiene
                                                </label>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="mal_oclusion" id="angle1"
                                                       value="1">
                                                <label class="form-check-label" for="angle1">
                                                    Angle I
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="mal_oclusion" id="angle2"
                                                       value="2">
                                                <label class="form-check-label" for="angle2">
                                                    Angle II
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="mal_oclusion" id="angle3"
                                                       value="3">
                                                <label class="form-check-label" for="angle3">
                                                    Angle III
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="mal_oclusion" id="mo_no_tiene"
                                                       value="0">
                                                <label class="form-check-label" for="mo_no_tiene">
                                                    No tiene
                                                </label>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="fluorosis" id="fl_leve"
                                                       value="1">
                                                <label class="form-check-label" for="fl_leve">
                                                    Leve
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="fluorosis" id="fl_moderada"
                                                       value="2">
                                                <label class="form-check-label" for="fl_moderada">
                                                    Moderada
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="fluorosis" id="fl_severa"
                                                       value="3">
                                                <label class="form-check-label" for="fl_severa">
                                                    Severa
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="fluorosis" id="fl_no_tiene"
                                                       value="0">
                                                <label class="form-check-label" for="fl_no_tiene">
                                                    No tiene
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>

                        <div id="step-6">
                            <form class="form-horizontal form-label-left">
                                <br><br>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">D</th>
                                        <th scope="col">C</th>
                                        <th scope="col">P</th>
                                        <th scope="col">O</th>
                                    <tbody>
                                    <tr>
                                        <td>

                                        </td>

                                        <td>
                                            <input type="number" min="0"  name="indice_c" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('indice_c') }}">
                                        </td>

                                        <td>
                                            <input type="number" min="0" name="indice_p" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('indice_p') }}">
                                        </td>

                                        <td>
                                            <input type="number" min="0" name="indice_o" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('indice_o') }}">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">d</th>
                                        <th scope="col">c</th>
                                        <th scope="col">e</th>
                                        <th scope="col">o</th>
                                    <tbody>
                                    <tr>
                                        <td>

                                        </td>

                                        <td>
                                            <input type="number" min="0" name="indice_c2" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('indice_c2') }}">
                                        </td>

                                        <td>
                                            <input type="number" min="0" name="indice_e2" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('indice_e2') }}">
                                        </td>

                                        <td>
                                            <input type="number" min="0" name="indice_o2" class="form-control mb-2 mr-sm-2"
                                                   value=" {{ old('indice_o2') }}">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            </form>
                        </div>



                    </div>
                    <!-- End SmartWizard Content -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<!-- jQuery Smart Wizard -->
<script src="{{ asset('js/wizard_primera_cita.js') }}"></script>
<script>
    function replaceAll(find, replace, str) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    function createOdontogram() {
        var htmlLecheLeft = "",
            htmlLecheRight = "",
            htmlLeft = "",
            htmlRight = "",
            a = 1;
        for (var i = 9 - 1; i >= 1; i--) {
            //Dientes Definitivos Cuandrante Derecho (Superior/Inferior)
            htmlRight += '<div data-name="value" id="dienteindex' + i + '" class="diente">' +
                '<span style="margin-left: 45px; margin-bottom:5px; display: inline-block !important; border-radius: 10px !important;" class="label label-info">index' + i + '</span>' +
                '<div id="tindex' + i + '" class="cuadro click">' +
                '</div>' +
                '<div id="lindex' + i + '" class="cuadro izquierdo click">' +
                '</div>' +
                '<div id="bindex' + i + '" class="cuadro debajo click">' +
                '</div>' +
                '<div id="rindex' + i + '" class="cuadro derecha click click">' +
                '</div>' +
                '<div id="cindex' + i + '" class="centro click">' +
                '</div>' +
                '</div>';
            //Dientes Definitivos Cuandrante Izquierdo (Superior/Inferior)
            htmlLeft += '<div id="dienteindex' + a + '" class="diente">' +
                '<span style="margin-left: 45px; margin-bottom:5px; display: inline-block !important; border-radius: 10px !important;" class="label label-info">index' + a + '</span>' +
                '<div id="tindex' + a + '" class="cuadro click">' +
                '</div>' +
                '<div id="lindex' + a + '" class="cuadro izquierdo click">' +
                '</div>' +
                '<div id="bindex' + a + '" class="cuadro debajo click">' +
                '</div>' +
                '<div id="rindex' + a + '" class="cuadro derecha click click">' +
                '</div>' +
                '<div id="cindex' + a + '" class="centro click">' +
                '</div>' +
                '</div>';
            if (i <= 5) {
                //Dientes Temporales Cuandrante Derecho (Superior/Inferior)
                htmlLecheRight += '<div id="dienteLindex' + i + '" style="left: -25%;" class="diente-leche">' +
                    '<span style="margin-left: 45px; margin-bottom:5px; display: inline-block !important; border-radius: 10px !important;" class="label label-primary">index' + i + '</span>' +
                    '<div id="tlecheindex' + i + '" class="cuadro-leche top-leche click">' +
                    '</div>' +
                    '<div id="llecheindex' + i + '" class="cuadro-leche izquierdo-leche click">' +
                    '</div>' +
                    '<div id="blecheindex' + i + '" class="cuadro-leche debajo-leche click">' +
                    '</div>' +
                    '<div id="rlecheindex' + i + '" class="cuadro-leche derecha-leche click click">' +
                    '</div>' +
                    '<div id="clecheindex' + i + '" class="centro-leche click">' +
                    '</div>' +
                    '</div>';
            }
            if (a < 6) {
                //Dientes Temporales Cuandrante Izquierdo (Superior/Inferior)
                htmlLecheLeft += '<div id="dienteLindex' + a + '" class="diente-leche">' +
                    '<span style="margin-left: 45px; margin-bottom:5px; display: inline-block !important; border-radius: 10px !important;" class="label label-primary">index' + a + '</span>' +
                    '<div id="tlecheindex' + a + '" class="cuadro-leche top-leche click">' +
                    '</div>' +
                    '<div id="llecheindex' + a + '" class="cuadro-leche izquierdo-leche click">' +
                    '</div>' +
                    '<div id="blecheindex' + a + '" class="cuadro-leche debajo-leche click">' +
                    '</div>' +
                    '<div id="rlecheindex' + a + '" class="cuadro-leche derecha-leche click click">' +
                    '</div>' +
                    '<div id="clecheindex' + a + '" class="centro-leche click">' +
                    '</div>' +
                    '</div>';
            }
            a++;
        }
        $("#tr").append(replaceAll('index', '1', htmlRight));
        $("#tl").append(replaceAll('index', '2', htmlLeft));
        $("#tlr").append(replaceAll('index', '5', htmlLecheRight));
        $("#tll").append(replaceAll('index', '6', htmlLecheLeft));


        $("#bl").append(replaceAll('index', '3', htmlLeft));
        $("#br").append(replaceAll('index', '4', htmlRight));
        $("#bll").append(replaceAll('index', '7', htmlLecheLeft));
        $("#blr").append(replaceAll('index', '8', htmlLecheRight));
    }
    var arrayPuente = [];
    var arrayPiezas = [];

    function eliminar_duplicados(id, diente, tratamiento) {
        console.log('tamano: ', arrayPiezas.length);
        var contador = 0;
        if (arrayPiezas.length == 0) {
            arrayPiezas.push({
                id: id,
                diente: diente.substr(0),
                tratamiento: tratamiento
            });
        } else {
            arrayPiezas.forEach(element => {
                console.log('contador', contador);
                console.log('duplicado', element);
                console.log('duplicado2 id', id);
                console.log('duplicado2 tratamiento', tratamiento);
                if (element.id != id || tratamiento != element.tratamiento) {
                    if (contador == 0) {
                        arrayPiezas.push({
                            id: id,
                            diente: diente.substr(0),
                            tratamiento: tratamiento
                        });
                    }

                    contador++;
                }
            });
        }
        console.log('arrayPiezas: ', arrayPiezas);
    }

    function desmarcar(id, diente, tratamiento) {
        arrayPiezas.forEach((element, index) => {
            console.log('desmarcar', id, diente, tratamiento);
            if (element.id == id && tratamiento == element.tratamiento) {
                arrayPiezas.splice(index, 1)
            }
        });
    }




    $(document).ready(function() {
        createOdontogram();
        $("#nuevo").click(function() {
            console.log(arrayPiezas);
        });

        var id =  $( "#my_id" ).val();

        function validacion(){
            var arrErrores = [];

            if( $("#problema_actual").val().length > 255 ) {
                arrErrores.push($("#problema_actual").attr("name"));
            }

            if( $("#patologia_text").val().length > 255 ) {
                arrErrores.push($("#patologia_text").attr("name"));
            }

            return arrErrores;
        }

        $("#guardarfinal").click(function() {
            var arrErr = validacion();

            if( arrErr.length == 0) {

                problema_actual = $("#problema_actual").val();

                presion_arterial = $("input[name=presion_arterial]").val();
                fr_cardiaca = $("input[name=fr_cardiaca]").val();
                temperatura = $("input[name=temperatura]").val();
                fr_respiratoria = $("input[name=fr_respiratoria]").val();

                labios = ($("input[type=checkbox][name=labios]:checked").val() == null) ? 0 : 1;
                mejillas = ($("input[type=checkbox][name=mejillas]:checked").val() == null) ? 0 : 1;
                maxilar_superior = ($("input[type=checkbox][name=maxilar_superior]:checked").val() == null) ? 0 : 1;
                maxilar_inferior = ($("input[type=checkbox][name=maxilar_inferior]:checked").val() == null) ? 0 : 1;
                lengua = ($("input[type=checkbox][name=lengua]:checked").val() == null) ? 0 : 1;
                paladar = ($("input[type=checkbox][name=paladar]:checked").val() == null) ? 0 : 1;
                piso = ($("input[type=checkbox][name=piso]:checked").val() == null) ? 0 : 1;
                carrillos = ($("input[type=checkbox][name=carrillos]:checked").val() == null) ? 0 : 1;
                glandulas_salivales = ($("input[type=checkbox][name=glandulas_salivales]:checked").val() == null) ? 0 : 1;
                faringe = ($("input[type=checkbox][name=faringe]:checked").val() == null) ? 0 : 1;
                atm = ($("input[type=checkbox][name=atm]:checked").val() == null) ? 0 : 1;
                ganglios = ($("input[type=checkbox][name=ganglios]:checked").val() == null) ? 0 : 1;
                patologia_text = $("#patologia_text").val();

                ArrPiezaFila1 = [];
                $("input:checkbox[name=muela_fila1]:checked").each(function () {
                    ArrPiezaFila1.push($(this).val());
                });
                ArrPiezaFila1 = (ArrPiezaFila1.length == 0) ? [0] : ArrPiezaFila1;
                placa_fila1 = $("input[name=placa_fila1]").val();
                calculo_fila1 = $("input[name=calculo_fila1]").val();
                gingivitis_fila1 = $("input[name=gingivitis_fila1]").val();

                ArrPiezaFila2 = [];
                $("input:checkbox[name=muela_fila2]:checked").each(function () {
                    ArrPiezaFila2.push($(this).val());
                });
                ArrPiezaFila2 = (ArrPiezaFila2.length == 0) ? [0] : ArrPiezaFila2;
                placa_fila2 = $("input[name=placa_fila2]").val();
                calculo_fila2 = $("input[name=calculo_fila2]").val();
                gingivitis_fila2 = $("input[name=gingivitis_fila2]").val();

                ArrPiezaFila3 = [];
                $("input:checkbox[name=muela_fila3]:checked").each(function () {
                    ArrPiezaFila3.push($(this).val());
                });
                ArrPiezaFila3 = (ArrPiezaFila3.length == 0) ? [0] : ArrPiezaFila3;
                placa_fila3 = $("input[name=placa_fila3]").val();
                calculo_fila3 = $("input[name=calculo_fila3]").val();
                gingivitis_fila3 = $("input[name=gingivitis_fila3]").val();

                ArrPiezaFila4 = [];
                $("input:checkbox[name=muela_fila4]:checked").each(function () {
                    ArrPiezaFila4.push($(this).val());
                });
                ArrPiezaFila4 = (ArrPiezaFila4.length == 0) ? [0] : ArrPiezaFila4;
                placa_fila4 = $("input[name=placa_fila4]").val();
                calculo_fila4 = $("input[name=calculo_fila4]").val();
                gingivitis_fila4 = $("input[name=gingivitis_fila4]").val();

                ArrPiezaFila5 = [];
                $("input:checkbox[name=muela_fila5]:checked").each(function () {
                    ArrPiezaFila5.push($(this).val());
                });
                ArrPiezaFila5 = (ArrPiezaFila5.length == 0) ? [0] : ArrPiezaFila5;
                placa_fila5 = $("input[name=placa_fila5]").val();
                calculo_fila5 = $("input[name=calculo_fila5]").val();
                gingivitis_fila5 = $("input[name=gingivitis_fila5]").val();

                ArrPiezaFila6 = [];
                $("input:checkbox[name=muela_fila6]:checked").each(function () {
                    ArrPiezaFila6.push($(this).val());
                });
                ArrPiezaFila6 = (ArrPiezaFila6.length == 0) ? [0] : ArrPiezaFila6;
                placa_fila6 = $("input[name=placa_fila6]").val();
                calculo_fila6 = $("input[name=calculo_fila6]").val();
                gingivitis_fila6 = $("input[name=gingivitis_fila6]").val();

                enfermedad_periodontal = $("input[name=enfermedad_periodontal]:checked").val();
                enfermedad_periodontal = (enfermedad_periodontal == null) ? 0 : enfermedad_periodontal;
                mal_oclusion = $("input[name=mal_oclusion]:checked").val();
                mal_oclusion = (mal_oclusion == null) ? 0 : mal_oclusion;
                fluorosis = $("input[name=fluorosis]:checked").val();
                fluorosis = (fluorosis == null) ? 0 : fluorosis;

                indice_c = $("input[name=indice_c]").val();
                indice_p = $("input[name=indice_p]").val();
                indice_o = $("input[name=indice_o]").val();
                indice_c2 = $("input[name=indice_c2]").val();
                indice_e2 = $("input[name=indice_e2]").val();
                indice_o2 = $("input[name=indice_o2]").val();


                $.ajax({
                    url: '/detalle-paciente/' + id,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        problema_actual: problema_actual,
                        presion_arterial: presion_arterial,
                        fr_cardiaca: fr_cardiaca,
                        temperatura: temperatura,
                        fr_respiratoria: fr_respiratoria,

                        labios: labios,
                        mejillas: mejillas,
                        maxilar_superior: maxilar_superior,
                        maxilar_inferior: maxilar_inferior,
                        lengua: lengua,
                        paladar: paladar,
                        piso: piso,
                        carrillos: carrillos,
                        glandulas_salivales: glandulas_salivales,
                        faringe: faringe,
                        atm: atm,
                        ganglios: ganglios,
                        patologia_text: patologia_text,

                        muela_fila1: ArrPiezaFila1,
                        placa_fila1: placa_fila1,
                        calculo_fila1: calculo_fila1,
                        gingivitis_fila1: gingivitis_fila1,

                        muela_fila2: ArrPiezaFila2,
                        placa_fila2: placa_fila2,
                        calculo_fila2: calculo_fila2,
                        gingivitis_fila2: gingivitis_fila2,

                        muela_fila3: ArrPiezaFila3,
                        placa_fila3: placa_fila3,
                        calculo_fila3: calculo_fila3,
                        gingivitis_fila3: gingivitis_fila3,

                        muela_fila4: ArrPiezaFila4,
                        placa_fila4: placa_fila4,
                        calculo_fila4: calculo_fila4,
                        gingivitis_fila4: gingivitis_fila4,

                        muela_fila5: ArrPiezaFila5,
                        placa_fila5: placa_fila5,
                        calculo_fila5: calculo_fila5,
                        gingivitis_fila5: gingivitis_fila5,

                        muela_fila6: ArrPiezaFila6,
                        placa_fila6: placa_fila6,
                        calculo_fila6: calculo_fila6,
                        gingivitis_fila6: gingivitis_fila6,

                        enfermedad_periodontal: enfermedad_periodontal,
                        mal_oclusion: mal_oclusion,
                        fluorosis: fluorosis,

                        indice_c: indice_c,
                        indice_p: indice_p,
                        indice_o: indice_o,
                        indice_c2: indice_c2,
                        indice_e2: indice_e2,
                        indice_o2: indice_o2,

                        arrayPiezas: arrayPiezas,
                        arrayPuente: arrayPuente,

                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'PUT',
                    dataType: 'json',
                    success: function () {
                        Swal.fire(
                            'Bien!',
                            'historia clínica guardada con éxito',
                            'success'
                        )

                    },
                    error: function (xhr, status) {
                        Swal.fire({
                            icon: 'warning',
                            title: 'Oops...',
                            text: 'Disculpe, existió un problema',
                        })
                    },
                    /* complete: function(xhr, status) {
                        alert('Petición realizada');
                    } */
                });

            }else{

                arrErr.forEach(function(err) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: err+' no debe tener mas de 250 caracteres',
                    })

                });
            }

        });


        $(".click").click(function(event) {
            var control = $("#controls").children().find('.active').attr('id');
            var cuadro = $(this).find("input[name=cuadro]:hidden").val();
            console.log($(this).attr('id'))
            switch (control) {
                case "fractura":
                    if ($(this).hasClass("click-blue")) {
                        $(this).removeClass('click-blue');
                        $(this).addClass('click-red');
                        desmarcar($(this).attr('id'), $(this).attr('id').substr(1), 'restauracion');
                        eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'fractura');
                    } else {
                        if ($(this).hasClass("click-red")) {
                            $(this).removeClass('click-red');
                            desmarcar($(this).attr('id'), $(this).attr('id').substr(1), 'fractura');
                        } else {
                            $(this).addClass('click-red');
                            eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'fractura');
                        }
                    }
                    break;
                case "restauracion":
                    if ($(this).hasClass("click-red")) {
                        $(this).removeClass('click-red');
                        $(this).addClass('click-blue');
                        desmarcar($(this).attr('id'), $(this).attr('id').substr(1), 'fractura');
                        eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'restauracion');
                    } else {
                        if ($(this).hasClass("click-blue")) {
                            $(this).removeClass('click-blue');
                            desmarcar($(this).attr('id'), $(this).attr('id').substr(1), 'restauracion');
                        } else {
                            $(this).addClass('click-blue');
                            eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'restauracion');
                        }
                    }
                    break;
                case "extraccion":
                    var dientePosition = $(this).position();
                    console.log('this: ', $(this))
                    console.log('dienteposition', dientePosition)
                    $(this).parent().children().each(function(index, el) {
                        if ($(el).hasClass("click-delete")) {
                            $(this).removeClass('click-delete');
                            console.log('entro en delete');
                            if (index == 4) {
                                desmarcar($(this).attr('id'), $(this).attr('id').substr(1), 'extraida');
                            }

                        } else if ($(el).hasClass("click")) {
                            console.log('entro en click');
                            $(el).addClass('click-delete');
                            if (index == 4) {
                                eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'extraida');
                            }
                        }
                    });
                    break;
                case "extraer":
                    var dientePosition = $(this).position();

                    $(this).parent().children().each(function(index, el) {
                        console.log('------');
                        if ($(el).hasClass("centro") || $(el).hasClass("centro-leche")) {
                            $(this).parent().append('<i id="extraccion_img" style="color:red;" class="fa fa-times fa-3x fa-fw"></i>');
                            if ($(el).hasClass("centro")) {
                                //console.log($(this).parent().children("i"))
                                $(this).parent().children("i").css({
                                    "position": "absolute",
                                    "top": "24%",
                                    "width": "115"
                                });
                                eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'extraer');
                            } else {
                                $(this).parent().children("i").css({
                                    "position": "absolute",
                                    "top": "21%",
                                    "left": "1.2ex"
                                });
                                eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'extraer');
                            }
                            //
                        }
                    });
                    break;
                case "endodoncia":
                    var dientePosition = $(this).position();
                    //console.log($(this))
                    //console.log(dientePosition)
                    //console.log($(this).parent().children());
                    $(this).parent().children().each(function(index, el) {
                        console.log('------');
                        if ($(el).hasClass("centro") || $(el).hasClass("centro-leche")) {
                            $(this).parent().append('<i id="endodoncia_img" style="color:blue;" class="fa fa-caret-up fa-3x fa-fw"></i>');
                            if ($(el).hasClass("centro")) {
                                //console.log($(this).parent().children("i"))
                                $(this).parent().children("i").css({
                                    "position": "absolute",
                                    "top": "24%",
                                    "width": "115"
                                });
                                eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'endodoncia');
                            } else {
                                $(this).parent().children("i").css({
                                    "position": "absolute",
                                    "top": "21%",
                                    "left": "1.2ex"
                                });
                                eliminar_duplicados($(this).attr('id'), $(this).attr('id').substr(1), 'endodoncia');
                            }
                            //
                        }
                    });
                    break;
                case "puente":
                    var dientePosition = $(this).offset(),
                        leftPX;
                    console.log($(this)[0].offsetLeft)
                    var noDiente = $(this).parent().children().first().text();
                    var cuadrante = $(this).parent().parent().attr('id');
                    var left = 0,
                        width = 0;
                    if (arrayPuente.length < 1) {
                        $(this).parent().children('.cuadro').css('border-color', 'red');
                        arrayPuente.push({
                            diente: noDiente,
                            cuadrante: cuadrante,
                            left: $(this)[0].offsetLeft,
                            father: null
                        });
                    } else {
                        $(this).parent().children('.cuadro').css('border-color', 'red');
                        arrayPuente.push({
                            diente: noDiente,
                            cuadrante: cuadrante,
                            left: $(this)[0].offsetLeft,
                            father: arrayPuente[0].diente
                        });
                        var diferencia = Math.abs((parseInt(arrayPuente[1].diente) - parseInt(arrayPuente[1].father)));
                        if (diferencia == 1) width = diferencia * 60;
                        else width = diferencia * 50;

                        if (arrayPuente[0].cuadrante == arrayPuente[1].cuadrante) {
                            if (arrayPuente[0].cuadrante == 'tr' || arrayPuente[0].cuadrante == 'tlr' || arrayPuente[0].cuadrante == 'br' || arrayPuente[0].cuadrante == 'blr') {
                                if (arrayPuente[0].diente > arrayPuente[1].diente) {
                                    console.log(arrayPuente[0])
                                    leftPX = (parseInt(arrayPuente[0].left) + 10) + "px";
                                } else {
                                    leftPX = (parseInt(arrayPuente[1].left) + 10) + "px";
                                    //leftPX = "45px";
                                }
                            } else {
                                if (arrayPuente[0].diente < arrayPuente[1].diente) {
                                    leftPX = "-45px";
                                } else {
                                    leftPX = "45px";
                                }
                            }
                        }
                        console.log(leftPX);
                        /*$(this).parent().append('<div style="z-index: 9999; height: 5px; width:' + width + 'px;" id="puente" class="click-red"></div>');
                        $(this).parent().children().last().css({
                            "position": "absolute",
                            "top": "80px",
                            "left": "50px"
                        });*/
                        $(this).parent().append('<div style="z-index: 9999; height: 5px; width:' + width + 'px;" id="puente" class="click-red"></div>');
                        $(this).parent().children().last().css({
                            "position": "absolute",
                            "top": "80px",
                            "left": leftPX
                        });
                    }

                    break;
                default:
                    console.log("borrar case");
                    $('#extraccion_img').remove();
                    $('#endodoncia_img').remove();
                    desmarcar('c' + $(this).attr('id').substr(1), $(this).attr('id').substr(1), 'endodoncia');
                    desmarcar('c' + $(this).attr('id').substr(1), $(this).attr('id').substr(1), 'extraer');

            }
            return false;
        });
        return false;
    });
</script>
@endsection
