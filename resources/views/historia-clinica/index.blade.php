@extends('base')

@section('title', 'Historias clínicas')

@section('estilo')
    <link href="{{ asset('css/historia-style.css') }}" rel="stylesheet">
@endsection


@section('content')
    {{--<button class="btn btn-success"><a href='{{ route('primera.cita') }}'>Nuevo</a></button>--}}

        {{--<div class="alert alert-success">
            <h3 > Pacientes </h3>
        </div>--}}

    <h3 class="text-white alert-success pl-2 border" style="width: 150px"> Pacientes </h3>
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Paciente</th>
            <th scope="col">N° historia clínica</th>
            <th scope="col">Última visita</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>

        @foreach($pacientes as $paciente)
            <tr>

                <th scope="row">{{ $paciente->primer_nombre }} {{$paciente->primer_apellido}}</th>
                <td>{{ $paciente->cedula }}</td>
                <td> {{substr($paciente->updated_at,0, -9) }}</td>

                <td>
                    <a href='{{ route('detalle-paciente.edit',['detalle_paciente' => $paciente->detalle_paciente]) }}'>
                        <button type="button" class="btn btn-primary">
                            <i class="fas fa-pencil-alt"></i>
                        </button>
                    </a>

                    <a href='{{ route('detalle-paciente.show',['detalle_paciente' => $paciente->detalle_paciente]) }}'>
                        <button type="button" class="btn btn-primary">
                            <i class="fas fa-file-invoice"></i>
                        </button>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <nav>
        <ul class="pagination">
            <li class="page-item disabled">
                <a class="page-link" href="#">Previous</a>
            </li>
            <li class="page-item active">
                <a class="page-link" href="#">1</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">2</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">3</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="#">Next</a>
            </li>
        </ul>
    </nav>


@endsection

@section('scripts')

@endsection







