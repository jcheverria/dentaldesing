@extends('base')

@section('title', 'Historia clínica')

@section('estilo')
    <link href="{{ asset('css/historia-style.css') }}" rel="stylesheet">
@endsection


@section('content')


    <h3>Historia clínica del paciente</h3>
    <hr>
    <p>aquí el paciente podrá ver su historia clínica en pdf o eviársela a su correo</p>

    <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Elija un paciente
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">Hijo</a>
            <a class="dropdown-item" href="#">Hija</a>
            <a class="dropdown-item" href="#">Abuelita</a>
        </div>
    </div>
    <button class="btn btn-success"><a> Enviar </a></button>

@endsection

@section('scripts')

@endsection







