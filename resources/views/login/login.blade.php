<!DOCTYPE html>
<html lang="en">
@extends('template.head-login')

@section('estilo')
<link href="{{ asset('css/login-style.css') }}" rel="stylesheet">
@endsection

@section('title', 'Registro')

<body>


    <div class="logo">
        <img class="img-logo" src="img/logo/logo.jpeg">
    </div>

    <h3 class="title">Ingreso</h3>

    {{--
<div class="container">

    <form action="{{ route('historia.index') }}" method="get" enctype="multipart/form-data">
    <div class="col-sm-3"></div>

    <div class="form-group row">
        <label class=" col-form-label">Cédula:</label>
        <input type="text" class="form-control">

        <label>Contraseña:</label>
        <input type="password" class="form-control">

    </div>

    <div class="col-sm-3"></div>
    <button class="btn btn-primary" type="submit">Ingresar</button>
    <a href='{{ route('registro') }}'>Registrarse</a>
    </form>


    </div>
    --}}



    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Cédula') }}</label>

            <div class="col-md-4">
                <input id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

            <div class="col-md-4">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>


        <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Login') }}
                </button>
                <a href='{{ route('registro') }}'>Registrarse</a>
            </div>
        </div>
    </form>




    <!-- script -->
    @include('template.scripts')

    <!-- /script -->

</body>

</html>
