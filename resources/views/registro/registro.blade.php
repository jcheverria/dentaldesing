<!DOCTYPE html>
<html lang="en">
@extends('template.head-login')

@section('estilo')
    <link href="{{ asset('css/registro-style.css') }}" rel="stylesheet">
@endsection

@section('title', 'Registro')
<body>


<div class="logo-pequeno-container ">
    <img class="logo-pequeno" src="{{ asset('img/logo/logo.jpeg') }}">
</div>
<hr>

<div class="contenedor">
    <h3 class="titulo-registro">Registrarse en Dental Design</h3>
    <a class="atras" href='{{ route('login-dental') }}'><i class="fas fa-arrow-circle-left fa-2x"></i></a>

</div>

@if ($errors->any())

    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li> {{ $error }} </li>
            @endforeach
        </ul>
    </div>
    </ul>
@endif

<div class="formulario">


    <form method="POST" action=" {{ route('paciente.store') }}">
        {{ csrf_field() }}

        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text" id="">Nombres</span>
            </div>
            <input class="form-control" type="text" name="primer_nombre" value="{{ old('primer_nombre')}}" placeholder="Primer nombre" >
            <input class="form-control" type="text" name="segundo_nombre"  value="{{ old('segundo_nombre')}}" placeholder="Segundo nombre">
        </div>

        <div class="input-group mt-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="">Apellidos</span>
            </div>
            <input class="form-control" type="text" name="primer_apellido" value="{{ old('primer_apellido') ?? ''}}" placeholder="Paterno" >
            <input class="form-control" type="text" name="segundo_apellido"  value="{{ old('segundo_apellido') ?? ''}}" placeholder="Materno">
        </div>

        <div class="input-group mt-3">
            <div class="input-group-prepend">
                <i class="input-group-text far fa-id-card"></i>
            </div>
            <input type="text" name="cedula" class="form-control" placeholder="cédula" value="{{ old('cedula') }}">
        </div>

        <div class="input-group mt-3">
            <div class="input-group-prepend">
                <i class="input-group-text far fa-envelope"></i>
            </div>
            <input type="email" name="email" class="form-control" placeholder="email" value=" {{ old('email') }}">
        </div>

        <div class="input-group mt-3">
            <div class="input-group-prepend">
                <i class="input-group-text"><img  src="{{asset('img/flaticon/llave-inteligente.svg')}}" height="20px"></i>
            </div>
            <input type="password" name="password" class="form-control" value="{{ old('password') }}" placeholder="Contraseña">
        </div>

        <div class="input-group mt-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="">Edad</span>
            </div>
            <input class="form-control" type="text" name="edad" value="{{ old('edad')}}" placeholder="Edad" >
        </div>

        <div class="form-group">
            <label>Género:</label>
            <select name="sexo" class="form-control" id="selectGenero">
                <option value="M">Masculino</option>
                <option value="F">Femenino</option>
                <option value="O">Prefiero no decir</option>
            </select>
        </div>

        <div class="form-group">
            <label>Motivo de la consulta:</label>
            <textarea name="motivo_consulta" class="form-control" rows="3">{{ old('motivo_consulta') }}</textarea>
        </div>

        {{-- checkbox --}}



        <div class="form-group">
            <label>Antecedentes personales o familiares:</label>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-check">
                        <input class="form-check-input" type="hidden" value="0" id="antibiotico" name="antibiotico">
                        <input class="form-check-input" type="checkbox" value="1" id="antibiotico" name="antibiotico">
                        <label class="form-check-label" for="antibiotico">
                            Alergia a antibiótico
                        </label>
                    </div>

                    <div class="form-check">
                        <input class="form-check-input" type="hidden" value="0" id="anestesia" name="anestesia">
                        <input class="form-check-input" type="checkbox" value="1" id="anestesia" name="anestesia">
                        <label class="form-check-label" for="anestesia">
                            Alergia a anestesia
                        </label>
                    </div>

                    <div class="form-check">
                        <input class="form-check-input" type="hidden" value="0" id="hemorragias" name="hemorragias">
                        <input class="form-check-input" type="checkbox" value="1" id="hemorragias" name="hemorragias">
                        <label class="form-check-label" for="hemorragias">
                            Hemorragias
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="hidden" value="0" id="sida" name="sida">
                        <input class="form-check-input" type="checkbox" value="1" id="sida" name="sida">
                        <label class="form-check-label" for="sida">
                            VIH/SIDA
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="hidden" value="0" id="tuberculosis" name="tuberculosis">
                        <input class="form-check-input" type="checkbox" value="1" id="tuberculosis" name="tuberculosis">
                        <label class="form-check-label" for="tuberculosis">
                            Tuberculosis
                        </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-check">
                        <input class="form-check-input" type="hidden" value="0" id="asma" name="asma">
                        <input class="form-check-input" type="checkbox" value="1" id="asma" name="asma">
                        <label class="form-check-label" for="asma">
                            Asma
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="hidden" value="0" id="diabetes" name="diabetes">
                        <input class="form-check-input" type="checkbox" value="1" id="diabetes" name="diabetes">
                        <label class="form-check-label" for="diabetes">
                            Diabetes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="hidden" value="0" id="hipertension" name="hipertension">
                        <input class="form-check-input" type="checkbox" value="1" id="hipertension" name="hipertension">
                        <label class="form-check-label" for="hipertension">
                            Hipertensión
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="hidden" value="0" id="cardiacas" name="cardiacas">
                        <input class="form-check-input" type="checkbox" value="1" id="cardiacas" name="cardiacas">
                        <label class="form-check-label" for="cardiacas">
                            Enfermedades cardiacas
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="hidden" value="0" id="otros" name="otros">
                        <input class="form-check-input" type="checkbox" value="1" id="otros" name="otros">
                        <label class="form-check-label" for="otros">
                            Otros
                        </label>
                    </div>
                </div>
            </div>




            {{--<div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <input type="checkbox">
                    </div>
                </div>

                <input type="text" class="form-control" placeholder="otro">
            </div>--}}

        </div>


        {{--<button type="submit" class="btn btn-primary">Crear Usuario</button>--}}
        <button type="submit" class="btn btn-primary btn-block">Crear Usuario</button>

    </form>
</div>


<!-- script -->
@include('template.scripts')

<!-- /script -->

</body>
</html>
