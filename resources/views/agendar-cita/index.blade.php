@extends('base')

@section('title', 'Historia clínica')

@section('estilo')
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />
@endsection
@section('content')
<div>
    <input type="text" id="fecha_hora" hidden>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h4>Agendar Cita</h4>
                <br>
                Ingrese descripcion de la cita: <input type="text" class="form-control" name="" id="decripcion_cita">
                <p>Desea agendar una cita?</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <input id="save_cita" type="button" class="btn btn-primary" id="appointment_update" value="Guardar">
            </div>
        </div>
    </div>
</div>
<div id='calendar'></div>

@endsection
@section('scripts')
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>
<script>
    $("#save_cita").click(function() {
        //console.log($("#fecha_hora").val(), '---');
        var fecha_hora = $("#fecha_hora").val();
        var decripcion_cita = $("#decripcion_cita").val();
        console.log(String(fecha_hora), '---');
        $.ajax({
            url: '/cita/insertar',
            data: {
                fecha_hora: fecha_hora,
                decripcion_cita: decripcion_cita
            },
            type: 'GET',
            success: function(json) {
                var myCalendar = $('#calendar');
                myCalendar.fullCalendar();
                var myEvent = {
                    title: decripcion_cita,
                    //allDay: true,
                    start: moment(fecha_hora),
                    //end: '2020-08-03 08:30'
                };
                myCalendar.fullCalendar('renderEvent', myEvent);
                //console.log(moment(fecha_hora).format('YYYY-MM-DD hh:mm'));

            }
        });
    });
    // page is now ready, initialize the calendar...
    $('#calendar').fullCalendar({
        /* defaultView: 'agendaWeek',
        lang: 'es',
        columnFormat: 'dddd',
        weekend: false,
        selectable: true,
        header: {
            left: 'prev,next today',
            center: 'addEventButton',
            right: 'month,agendaWeek,agendaDay'
        },
        customButtons: {
            addEventButton: {
                text: 'add event...',
                click: function() {
                    var dateStr = prompt('Enter a date in YYYY-MM-DD format');
                    var date = moment(dateStr);

                    if (date.isValid()) {
                        $('#calendar').fullCalendar('renderEvent', {
                            title: 'dynamic event',
                            start: date,
                            allDay: true
                        });
                        alert('Great. Now, update your database...');
                    } else {
                        alert('Invalid date.');
                    }
                }
            }
        }, */
        defaultView: 'agendaWeek',
        lang: 'es',
        weekends: false,
        columnFormat: 'dddd',
        selectable: true,
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sabado'],
        header: {
            left: 'prev,next today',
            center: 'title',
            //right: 'month,agendaWeek,agendaDay'
        },
        defaultDate: '2020-08-03',
        editable: true,
        eventLimit: true,
        weekend: true,
        allDaySlot: false,
        minTime: '08:00:00',
        maxTime: '19:00:00',
        slotDuration: '00:30:00',
        slotLabelInterval: '00:30:00',
        /* eventClick: function(calEvent, jsEvent, view) {

            $('#editModal').modal();
            alert('selected ' + info.startStr + ' to ' + info.endStr);
        }, */
        select: function(info) {
            console.log('info: ', info);
            $("#fecha_hora").val(moment(info).format('YYYY-MM-DD h:mm:ss'));
            $('#editModal').modal();
        },
        /* events: [{
            id: 'a',
            title: 'my event',
            start: '2020-08-03 08:00',
            end: '2020-08-03 80:30'
        }], */

    });
</script>
@endsection