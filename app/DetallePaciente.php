<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetallePaciente extends Model
{
    protected $table = 'detalles_paciente';

    protected $fillable = [
        'paciente_id',
        'motivo_consulta',
        'problema_actual',
        'antecedentes',
        'presion_arterial',
        'frecuencia_cardiaca',
        'temperatura',
        'frecuencia_respiratoria',
        'patologias',
        'patologias_text',
        'periodontal',
        'oclusion',
        'fluorosis',
        'pieza1',
        'placa1',
        'calculo1',
        'gingivitis1',

        'pieza2',
        'placa2',
        'calculo2',
        'gingivitis2',

        'pieza3',
        'placa3',
        'calculo3',
        'gingivitis3',

        'pieza4',
        'placa4',
        'calculo4',
        'gingivitis4',

        'pieza5',
        'placa5',
        'calculo5',
        'gingivitis5',

        'pieza6',
        'placa6',
        'calculo6',
        'gingivitis6',

        'indice_c',
        'indice_p',
        'indice_o',
        'indice_c2',
        'indice_e2',
        'indice_o2'
    ];


    public function paciente()
    {
        return $this->belongsTo('App\Paciente','paciente_id');
    }
}
