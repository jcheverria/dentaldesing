<?php

namespace App\Http\Controllers;

use App\DetallePaciente;
use App\Paciente;
use App\User;
use DB;
use Hash;
use Illuminate\Http\Request;


class PacienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$pacientes = Paciente::all();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('registro.registro');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arr_antecedentes = [];

        $data = $request->validate([
            'primer_nombre' => 'required | max:255' ,
            'segundo_nombre' => 'required | max:255',
            'primer_apellido' => 'required | max:255' ,
            'segundo_apellido' => 'required | max:255' ,
            'cedula' => 'required | max:255|unique:pacientes,cedula' ,
            'email' => 'required | email|unique:pacientes,email',
            'password' => 'required | max:255' ,
            'edad' => 'required | numeric' ,
            'sexo' => 'max:255',
            'motivo_consulta' => 'max:255',
            'antibiotico' => '',
            'anestesia' => '',
            'hemorragias' => '',
            'sida' => '',
            'tuberculosis' => '' ,
            'asma' => '',
            'diabetes' => '',
            'hipertension' => '',
            'cardiacas' => '',
            'otros' => ''

        ]);

        $arr_antecedentes['antibiotico'] = $data['antibiotico'];
        $arr_antecedentes['anestesia'] = $data['anestesia'];
        $arr_antecedentes['hemorragias'] = $data['hemorragias'];
        $arr_antecedentes['sida'] = $data['sida'];
        $arr_antecedentes['tuberculosis'] = $data['tuberculosis'];
        $arr_antecedentes['asma'] = $data['asma'];
        $arr_antecedentes['diabetes'] = $data['diabetes'];
        $arr_antecedentes['hipertension'] = $data['hipertension'];
        $arr_antecedentes['otros'] = $data['otros'];

        $string_antecedentes= json_encode($arr_antecedentes);

       $paciente = Paciente::create([
            'primer_nombre' => $data['primer_nombre'],
            'segundo_nombre' => $data['segundo_nombre'],
            'primer_apellido' => $data['primer_apellido'],
            'segundo_apellido' => $data['segundo_apellido'],
            'cedula' => $data['cedula'],
            'email' => $data['email'],
            'password' => bcrypt( $data['password']),
            'edad' => $data['edad'],
            'sexo' => $data['sexo'],

        ]);

        DetallePaciente::create([
            'paciente_id' => $paciente->id,
            'motivo_consulta' => $data['motivo_consulta'],
            'antecedentes' => $string_antecedentes,

            ]);

        User::create([
            'name' => $data['primer_nombre'],
            'email' => $data['cedula'],
            'password' => bcrypt( $data['password'])
        ])->assign('paciente');

        return redirect()->route('login-dental');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
