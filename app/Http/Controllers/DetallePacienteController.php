<?php

namespace App\Http\Controllers;

use App\DetallePaciente;
use App\Paciente;
use DB;
use Illuminate\Http\Request;

class DetallePacienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pacientes = Paciente::all();

        return view('historia-clinica.index', ['pacientes' => $pacientes]);
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paciente = Paciente::find($id);

        return view('historia-clinica.show', ['paciente' => $paciente, 'detalle_paciente' => $paciente->detalle_paciente ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paciente = Paciente::find($id);

        return view('historia-clinica.edit', ['paciente' => $paciente, 'detalle_paciente' => $paciente->detalle_paciente ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $arr_patologias = [];
        $data = request()->all();
        $detalle_paciente = DetallePaciente::find($id);

        $arr_patologias['labios'] = $data['labios'];
        $arr_patologias['mejillas'] = $data['mejillas'];
        $arr_patologias['maxilar_superior'] = $data['maxilar_superior'];
        $arr_patologias['maxilar_inferior'] = $data['maxilar_inferior'];
        $arr_patologias['lengua'] = $data['lengua'];
        $arr_patologias['paladar'] = $data['paladar'];
        $arr_patologias['piso'] = $data['piso'];
        $arr_patologias['carrillos'] = $data['carrillos'];
        $arr_patologias['glandulas_salivales'] = $data['glandulas_salivales'];
        $arr_patologias['faringe'] = $data['faringe'];
        $arr_patologias['atm'] = $data['atm'];
        $arr_patologias['ganglios'] = $data['ganglios'];

        $json_patologias = json_encode($arr_patologias);

        $detalle_paciente->problema_actual = $data['problema_actual'];
        $detalle_paciente->presion_arterial = $data['presion_arterial'];
        $detalle_paciente->frecuencia_cardiaca = $data['fr_cardiaca'];
        $detalle_paciente->temperatura = $data['temperatura'];
        $detalle_paciente->frecuencia_respiratoria = $data['fr_respiratoria'];
        $detalle_paciente->patologias = $json_patologias;
        $detalle_paciente->patologias_text = $data['patologia_text'];
        $detalle_paciente->periodontal = $data['enfermedad_periodontal'];
        $detalle_paciente->oclusion = $data['mal_oclusion'];
        $detalle_paciente->fluorosis = $data['fluorosis'];

        $detalle_paciente->piezas1 = $data['muela_fila1'];
        $detalle_paciente->placa1 = $data['placa_fila1'];
        $detalle_paciente->calculo1 = $data['calculo_fila1'];
        $detalle_paciente->gingivitis1 = $data['gingivitis_fila1'];

        $detalle_paciente->piezas2 = $data['muela_fila2'];
        $detalle_paciente->placa2 = $data['placa_fila2'];
        $detalle_paciente->calculo2 = $data['calculo_fila2'];
        $detalle_paciente->gingivitis2 = $data['gingivitis_fila2'];

        $detalle_paciente->piezas3 = $data['muela_fila3'];
        $detalle_paciente->placa3 = $data['placa_fila3'];
        $detalle_paciente->calculo3 = $data['calculo_fila3'];
        $detalle_paciente->gingivitis3 = $data['gingivitis_fila3'];

        $detalle_paciente->piezas4 = $data['muela_fila4'];
        $detalle_paciente->placa4 = $data['placa_fila4'];
        $detalle_paciente->calculo4 = $data['calculo_fila4'];
        $detalle_paciente->gingivitis4 = $data['gingivitis_fila4'];

        $detalle_paciente->piezas5 = $data['muela_fila5'];
        $detalle_paciente->placa5 = $data['placa_fila5'];
        $detalle_paciente->calculo5 = $data['calculo_fila5'];
        $detalle_paciente->gingivitis5 = $data['gingivitis_fila5'];

        $detalle_paciente->piezas6 = $data['muela_fila6'];
        $detalle_paciente->placa6 = $data['placa_fila6'];
        $detalle_paciente->calculo6 = $data['calculo_fila6'];
        $detalle_paciente->gingivitis6 = $data['gingivitis_fila6'];

        $detalle_paciente->indice_c = $data['indice_c'];
        $detalle_paciente->indice_p = $data['indice_p'];
        $detalle_paciente->indice_o = $data['indice_o'];
        $detalle_paciente->indice_c2 = $data['indice_c2'];
        $detalle_paciente->indice_e2 = $data['indice_e2'];
        $detalle_paciente->indice_o2 = $data['indice_o2'];
        $detalle_paciente->save();

        $array_piezas = $request->arrayPiezas;
        $array_final = [];

        if ($array_piezas != null) {
            //cabeceras
            $cabeceras = ['inicio'];
            foreach ($array_piezas as  $d => $d1) {
                array_push($cabeceras, $d1['diente']);
            }
            $cabeceras = array_unique($cabeceras);

            //juntar arrays piezas dentales
            foreach ($array_piezas as  $key => $value) {
                $temp = [];
                $cont = 0;
                foreach ($array_piezas as  $k => $v) {
                    if ($value['diente'] == $v['diente']) {
                        array_push($temp, $v);
                    }
                }
                if (array_search($value['diente'], $cabeceras) > 0) {
                    array_push($array_final, $temp);
                }
                unset($cabeceras[array_search($value['diente'], $cabeceras)]);
            }

            $array_base = ['id_detalle_paciente' => 1];
            $array_final = array_values($array_final);

            foreach ($array_final as $ka1 => $va1) {
                foreach ($va1 as $wk1 => $wv1) {
                    $pieza = 'pieza_' . $wv1['diente'];
                }
                $array_base = array_merge($array_base, [$pieza => json_encode($va1)]);
            }

            DB::table('detalle_odontograma')->insert(
                $array_base

            );
        }


       return response()->json(['success' => 'success'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function primera_cita_store(Request $request)
    {
    }
}
