<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    //
    protected $fillable = [
        'descripcion',
        'id_paciente',
        'id_paciente_agendamiento',
        'horario_cita',
    ];
    const CREATED_AT = 'creacion';
    const UPDATED_AT = 'actualizacion';
}
