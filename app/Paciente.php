<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $table = 'pacientes';

    protected $fillable = [
        'primer_nombre',
        'segundo_nombre',
        'primer_apellido',
        'segundo_apellido',
        'cedula',
        'email',
        'password',
        'edad',
        'sexo',
        ];


    public function detalle_paciente()
    {
        return $this->hasOne('App\DetallePaciente','paciente_id');
    }
}
