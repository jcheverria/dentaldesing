<?php

use Illuminate\Database\Seeder;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //creo usuarios
        $admin = DB::table('users')->insert([
            'name' => 'Administrador',
            'email' => 'admin',
            'password' => Hash::make('admin123456'),
        ]);
        $paciente = DB::table('users')->insert([
            'name' => 'Paciente',
            'email' => 'paciente',
            'password' => Hash::make('paciente123456'),
        ]);
        $doctor = DB::table('users')->insert([
            'name' => 'Doctor',
            'email' => 'doctor',
            'password' => Hash::make('doctor123456'),
        ]);

        //bouncer
        $admin_role = Bouncer::role()->firstOrCreate([
            'name' => 'admin',
            'title' => 'Administrator',
        ]);
        $paciente_role = Bouncer::role()->firstOrCreate([
            'name' => 'Paciente',
            'title' => 'Paciente',
        ]);
        $doctor_role = Bouncer::role()->firstOrCreate([
            'name' => 'Doctor',
            'title' => 'doctor',
        ]);

        $llenarHistoria = Bouncer::ability()->firstOrCreate([ //$createPrimeraCita
            'name' => 'llenar-historia',
            'title' => 'Llenar Historia clínica',
        ]);
        $createAgendarCita = Bouncer::ability()->firstOrCreate([
            'name' => 'agendar-cita',
            'title' => 'Agendar cita',
        ]);
        $verHistoriaClinica = Bouncer::ability()->firstOrCreate([
            'name' => 'ver-historia-clinica',
            'title' => 'Ver historia Clinica',
        ]);
        //asigno habilidad a los roles
        Bouncer::allow($admin_role)->to($llenarHistoria);
        Bouncer::allow($admin_role)->to($createAgendarCita);
        Bouncer::allow($admin_role)->to($verHistoriaClinica);

        Bouncer::allow($paciente_role)->to($createAgendarCita);

        Bouncer::allow($doctor_role)->to($verHistoriaClinica);
        Bouncer::allow($doctor_role)->to($llenarHistoria);

        (\App\User::where('name', 'Administrador')->first())->assign('admin');
        (\App\User::where('name', 'Paciente')->first())->assign('paciente');
        (\App\User::where('name', 'Doctor')->first())->assign('doctor');

        //ASIGNO HABILIDAD AL ADMIN
        Bouncer::allow($admin)->to('llenar-historia');
        Bouncer::allow($admin)->to('agendar-cita');
        Bouncer::allow($admin)->to('ver-historia-clinica');

        //ASIGNO HABILIDAD AL DOCTOR
        Bouncer::allow($doctor)->to('llenar-historia');

        //ASIGNO HABILIDAD AL PACIENTE
        Bouncer::allow($paciente)->to('ver-historia-clinica');
    }
}
