<?php

use Illuminate\Database\Seeder;
use App\Paciente;
use App\DetallePaciente;
Use App\User;

class PacienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Paciente::create([
            'primer_nombre' => 'María',
            'segundo_nombre' => 'Leticia',
            'primer_apellido' => 'Godoy',
            'segundo_apellido' => 'Ruiz',
            'cedula' => '1102450000',
            'email' => 'letydeperalta@gmail.com',
            'password' => '12345678',
            'edad' => '50',
            'sexo' => 'F',

        ]);

        DetallePaciente::create([
            'motivo_consulta' => 'dolor de muela',
            'antecedentes' => '{"asma": "0", "sida": "0", "otros": "0", "diabetes": "0", "anestesia": "0",
                                "antibiotico": "1", "hemorragias": "0", "hipertension": "0", "tuberculosis": "0"}',
            'paciente_id' => 1
        ]);

        User::create([
            'name' => 'María',
            'email' => '1102450000',
            'password' => bcrypt('12345678')
        ])->assign('paciente');
    }
}
