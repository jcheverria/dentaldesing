<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Faker\Generator as Faker;

$factory->define(\App\Paciente::class, function (Faker $faker) {
    return [

            'primer_nombre' => $faker->name,
            'segundo_nombre' => $faker->name,
            'primer_apellido' => $faker->lastName,
            'segundo_apellido' => $faker->lastName,
            'cedula' => $faker->bankAccountNumber,
            'email' => $faker->email,
            'password' => bcrypt('secret'),
            'edad' => 30,
            'sexo' => 'M',
            'motivo_consulta' => 'Me duele la mejilla',

    ];
});
