<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDetallesPaciente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalles_paciente', function (Blueprint $table) {

            $table->id();
            $table->bigInteger('paciente_id')->unsigned();
            $table->string('motivo_consulta')->nullable(); /* se esribe en lenguaje del paciente, porqué va a la consulta */
            $table->string('problema_actual')->nullable(); /*cronología, localización, características, intensidad, causa aparente, síntomas asociados, evolución, estado actual.*/
            $table->json('antecedentes')->nullable() ; /* check box para sida, diabetes, etc.*/

            $table->string('presion_arterial', 30)->nullable();
            $table->string('frecuencia_cardiaca', 30)->nullable();
            $table->string('temperatura', 30)->nullable();
            $table->string('frecuencia_respiratoria', 30)->nullable();

            $table->json('patologias')->nullable(); /*examen del sistema estomatognatico: labios, mejillas, etc.*/
            $table->string('patologias_text')->nullable();
            /*$table->integer('odontograma_id')->unsigned();
            $table->foreign('odontograma_id')->references('id')->on('odontogramas');*/
            $table->string('piezas1')->nullable();
            $table->tinyInteger('placa1')->nullable();
            $table->tinyInteger('calculo1')->nullable();
            $table->tinyInteger('gingivitis1')->nullable();

            $table->string('piezas2')->nullable();
            $table->tinyInteger('placa2')->nullable();
            $table->tinyInteger('calculo2')->nullable();
            $table->tinyInteger('gingivitis2')->nullable();

            $table->string('piezas3')->nullable();
            $table->tinyInteger('placa3')->nullable();
            $table->tinyInteger('calculo3')->nullable();
            $table->tinyInteger('gingivitis3')->nullable();

            $table->string('piezas4')->nullable();
            $table->tinyInteger('placa4')->nullable();
            $table->tinyInteger('calculo4')->nullable();
            $table->tinyInteger('gingivitis4')->nullable();

            $table->string('piezas5')->nullable();
            $table->tinyInteger('placa5')->nullable();
            $table->tinyInteger('calculo5')->nullable();
            $table->tinyInteger('gingivitis5')->nullable();

            $table->string('piezas6')->nullable();
            $table->tinyInteger('placa6')->nullable();
            $table->tinyInteger('calculo6')->nullable();
            $table->tinyInteger('gingivitis6')->nullable();

            $table->tinyInteger('periodontal')->nullable();
            $table->tinyInteger('oclusion')->nullable();
            $table->tinyInteger('fluorosis')->nullable();

            $table->tinyInteger('indice_c')->nullable();
            $table->tinyInteger('indice_p')->nullable();
            $table->tinyInteger('indice_o')->nullable();
            $table->tinyInteger('indice_c2')->nullable();
            $table->tinyInteger('indice_e2')->nullable();
            $table->tinyInteger('indice_o2')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalles_paciente');
    }
}
