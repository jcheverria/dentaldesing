<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleOdontogramaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_odontograma', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_detalle_paciente');
            //primera
            $table->json('pieza_18');
            $table->json('pieza_17');
            $table->json('pieza_16');
            $table->json('pieza_15');
            $table->json('pieza_14');
            $table->json('pieza_13');
            $table->json('pieza_12');
            $table->json('pieza_11');
            //segunda
            $table->json('pieza_21');
            $table->json('pieza_22');
            $table->json('pieza_23');
            $table->json('pieza_24');
            $table->json('pieza_25');
            $table->json('pieza_26');
            $table->json('pieza_27');
            $table->json('pieza_28');
            //tercera
            $table->json('pieza_48');
            $table->json('pieza_47');
            $table->json('pieza_46');
            $table->json('pieza_45');
            $table->json('pieza_44');
            $table->json('pieza_43');
            $table->json('pieza_42');
            $table->json('pieza_41');
            //cuarta
            $table->json('pieza_31');
            $table->json('pieza_32');
            $table->json('pieza_33');
            $table->json('pieza_34');
            $table->json('pieza_35');
            $table->json('pieza_36');
            $table->json('pieza_37');
            $table->json('pieza_38');
            //priemra de leche
            $table->json('pieza_55');
            $table->json('pieza_54');
            $table->json('pieza_53');
            $table->json('pieza_52');
            $table->json('pieza_51');
            //segunda de leche
            $table->json('pieza_61');
            $table->json('pieza_62');
            $table->json('pieza_63');
            $table->json('pieza_64');
            $table->json('pieza_65');
            //tercera de leche
            $table->json('pieza_85');
            $table->json('pieza_84');
            $table->json('pieza_83');
            $table->json('pieza_82');
            $table->json('pieza_81');
            //tercera de leche
            $table->json('pieza_71');
            $table->json('pieza_72');
            $table->json('pieza_73');
            $table->json('pieza_74');
            $table->json('pieza_75');
            $table->dateTime('creacion');
            $table->dateTime('actualizacion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_odontogramas');
    }
}
