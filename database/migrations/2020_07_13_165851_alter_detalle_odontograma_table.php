<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDetalleOdontogramaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement('ALTER TABLE `dental`.`detalle_odontograma` 
MODIFY COLUMN `pieza_18` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `id_detalle_paciente`,
MODIFY COLUMN `pieza_17` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_18`,
MODIFY COLUMN `pieza_16` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_17`,
MODIFY COLUMN `pieza_15` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_16`,
MODIFY COLUMN `pieza_14` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_15`,
MODIFY COLUMN `pieza_13` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_14`,
MODIFY COLUMN `pieza_12` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_13`,
MODIFY COLUMN `pieza_11` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_12`,
MODIFY COLUMN `pieza_21` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_11`,
MODIFY COLUMN `pieza_22` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_21`,
MODIFY COLUMN `pieza_23` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_22`,
MODIFY COLUMN `pieza_24` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_23`,
MODIFY COLUMN `pieza_25` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_24`,
MODIFY COLUMN `pieza_26` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_25`,
MODIFY COLUMN `pieza_27` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_26`,
MODIFY COLUMN `pieza_28` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_27`,
MODIFY COLUMN `pieza_48` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_28`,
MODIFY COLUMN `pieza_47` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_48`,
MODIFY COLUMN `pieza_46` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_47`,
MODIFY COLUMN `pieza_45` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_46`,
MODIFY COLUMN `pieza_44` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_45`,
MODIFY COLUMN `pieza_43` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_44`,
MODIFY COLUMN `pieza_42` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_43`,
MODIFY COLUMN `pieza_41` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_42`,
MODIFY COLUMN `pieza_31` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_41`,
MODIFY COLUMN `pieza_32` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_31`,
MODIFY COLUMN `pieza_33` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_32`,
MODIFY COLUMN `pieza_34` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_33`,
MODIFY COLUMN `pieza_35` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_34`,
MODIFY COLUMN `pieza_36` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_35`,
MODIFY COLUMN `pieza_37` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_36`,
MODIFY COLUMN `pieza_38` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_37`,
MODIFY COLUMN `pieza_55` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_38`,
MODIFY COLUMN `pieza_54` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_55`,
MODIFY COLUMN `pieza_53` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_54`,
MODIFY COLUMN `pieza_52` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_53`,
MODIFY COLUMN `pieza_51` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_52`,
MODIFY COLUMN `pieza_61` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_51`,
MODIFY COLUMN `pieza_62` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_61`,
MODIFY COLUMN `pieza_63` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_62`,
MODIFY COLUMN `pieza_64` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_63`,
MODIFY COLUMN `pieza_65` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_64`,
MODIFY COLUMN `pieza_85` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_65`,
MODIFY COLUMN `pieza_84` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_85`,
MODIFY COLUMN `pieza_83` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_84`,
MODIFY COLUMN `pieza_82` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_83`,
MODIFY COLUMN `pieza_81` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_82`,
MODIFY COLUMN `pieza_71` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_81`,
MODIFY COLUMN `pieza_72` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_71`,
MODIFY COLUMN `pieza_73` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_72`,
MODIFY COLUMN `pieza_74` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_73`,
MODIFY COLUMN `pieza_75` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL AFTER `pieza_74`,
MODIFY COLUMN `creacion` datetime(0) NULL AFTER `pieza_75`;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
