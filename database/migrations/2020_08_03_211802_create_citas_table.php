<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citas', function (Blueprint $table) {
            $table->id();
            $table->text('descripcion');
            $table->bigInteger('id_paciente')->unsigned();
            $table->bigInteger('id_paciente_agendamiento')->unsigned();
            $table->dateTime('horario_cita');
            $table->dateTime('creacion');
            $table->dateTime('actualizacion')->nullable();

            //
            $table->foreign('id_paciente')->references('id')->on('pacientes');
            $table->foreign('id_paciente_agendamiento')->references('id')->on('pacientes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citas');
    }
}
